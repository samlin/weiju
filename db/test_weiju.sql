/*
 Navicat Premium Data Transfer

 Source Server         : 微距
 Source Server Type    : MySQL
 Source Server Version : 50725
 Source Host           : rm-wz9v1jm0r587516sl8o.mysql.rds.aliyuncs.com:3306
 Source Schema         : test_weiju

 Target Server Type    : MySQL
 Target Server Version : 50725
 File Encoding         : 65001

 Date: 04/01/2020 19:00:18
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for act_evt_log
-- ----------------------------
DROP TABLE IF EXISTS `act_evt_log`;
CREATE TABLE `act_evt_log`  (
  `LOG_NR_` bigint(20) NOT NULL AUTO_INCREMENT,
  `TYPE_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TIME_STAMP_` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  `USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DATA_` longblob NULL,
  `LOCK_OWNER_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `LOCK_TIME_` timestamp(3) NULL DEFAULT NULL,
  `IS_PROCESSED_` tinyint(4) NULL DEFAULT 0,
  PRIMARY KEY (`LOG_NR_`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_ge_property
-- ----------------------------
DROP TABLE IF EXISTS `act_ge_property`;
CREATE TABLE `act_ge_property`  (
  `NAME_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VALUE_` varchar(300) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`NAME_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_ge_property
-- ----------------------------
INSERT INTO `act_ge_property` VALUES ('next.dbid', '295001', 119);
INSERT INTO `act_ge_property` VALUES ('schema.history', 'create(5.22.0.0)', 1);
INSERT INTO `act_ge_property` VALUES ('schema.version', '5.22.0.0', 1);

-- ----------------------------
-- Table structure for act_hi_actinst
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_actinst`;
CREATE TABLE `act_hi_actinst`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `ACT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CALL_PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ACT_NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ACT_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `ASSIGNEE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `START_TIME_` datetime(3) NOT NULL,
  `END_TIME_` datetime(3) NULL DEFAULT NULL,
  `DURATION_` bigint(20) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_ACT_INST_START`(`START_TIME_`) USING BTREE,
  INDEX `ACT_IDX_HI_ACT_INST_END`(`END_TIME_`) USING BTREE,
  INDEX `ACT_IDX_HI_ACT_INST_PROCINST`(`PROC_INST_ID_`, `ACT_ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_ACT_INST_EXEC`(`EXECUTION_ID_`, `ACT_ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_hi_actinst
-- ----------------------------
INSERT INTO `act_hi_actinst` VALUES ('247553', 'process_leave:1:247536', '247552', '247552', 'start', NULL, NULL, 'start', 'startEvent', NULL, '2018-06-14 23:44:03.000', '2018-06-14 23:44:03.000', 1, '');
INSERT INTO `act_hi_actinst` VALUES ('247559', 'process_leave:1:247536', '247552', '247552', 'user1', '247560', NULL, '经理审批', 'userTask', NULL, '2018-06-14 23:44:03.000', NULL, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('277506', 'process_leave:1:277504', '277505', '277505', 'start', NULL, NULL, 'start', 'startEvent', NULL, '2018-07-14 14:03:07.000', '2018-07-14 14:03:07.000', 15, '');
INSERT INTO `act_hi_actinst` VALUES ('277512', 'process_leave:1:277504', '277505', '277505', 'user1', '277513', NULL, '经理审批', 'userTask', 'd555ffd6b51f4df7a18e2ef4eece1bed', '2018-07-14 14:03:07.000', '2018-07-14 14:04:51.000', 104514, '');
INSERT INTO `act_hi_actinst` VALUES ('277524', 'process_leave:1:277504', '277505', '277505', 'sid-950749EA-4BB6-4276-B67A-DE03E34A0573', NULL, NULL, NULL, 'exclusiveGateway', NULL, '2018-07-14 14:04:51.000', '2018-07-14 14:04:51.000', 9, '');
INSERT INTO `act_hi_actinst` VALUES ('277526', 'process_leave:1:277504', '277505', '277525', 'child', NULL, NULL, 'subProcess', 'subProcess', NULL, '2018-07-14 14:04:51.000', NULL, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('277527', 'process_leave:1:277504', '277505', '277525', 'sstart', NULL, NULL, NULL, 'startEvent', NULL, '2018-07-14 14:04:51.000', '2018-07-14 14:04:51.000', 1, '');
INSERT INTO `act_hi_actinst` VALUES ('277528', 'process_leave:1:277504', '277505', '277525', 'allmanager', '277529', NULL, '总经理审批', 'userTask', '2211fec3e17c11e795ed201a068c6482', '2018-07-14 14:04:51.000', NULL, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('277536', 'process_leave:2:277534', '277535', '277535', 'start', NULL, NULL, 'start', 'startEvent', NULL, '2018-07-14 14:12:59.000', '2018-07-14 14:12:59.000', 1, '');
INSERT INTO `act_hi_actinst` VALUES ('277542', 'process_leave:2:277534', '277535', '277535', 'user1', '277543', NULL, '经理审批', 'userTask', 'd555ffd6b51f4df7a18e2ef4eece1bed', '2018-07-14 14:12:59.000', NULL, NULL, '');

-- ----------------------------
-- Table structure for act_hi_attachment
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_attachment`;
CREATE TABLE `act_hi_attachment`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DESCRIPTION_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `URL_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CONTENT_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TIME_` datetime(3) NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_hi_comment
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_comment`;
CREATE TABLE `act_hi_comment`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TIME_` datetime(3) NOT NULL,
  `USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ACTION_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `MESSAGE_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `FULL_MSG_` longblob NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_hi_detail
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_detail`;
CREATE TABLE `act_hi_detail`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ACT_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VAR_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `TIME_` datetime(3) NOT NULL,
  `BYTEARRAY_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DOUBLE_` double NULL DEFAULT NULL,
  `LONG_` bigint(20) NULL DEFAULT NULL,
  `TEXT_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TEXT2_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_DETAIL_PROC_INST`(`PROC_INST_ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_DETAIL_ACT_INST`(`ACT_INST_ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_DETAIL_TIME`(`TIME_`) USING BTREE,
  INDEX `ACT_IDX_HI_DETAIL_NAME`(`NAME_`) USING BTREE,
  INDEX `ACT_IDX_HI_DETAIL_TASK_ID`(`TASK_ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_hi_detail
-- ----------------------------
INSERT INTO `act_hi_detail` VALUES ('247558', 'VariableUpdate', '247552', '247552', NULL, '247553', 'userLeave', 'serializable', 0, '2018-06-14 23:44:03.000', '247557', NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_detail` VALUES ('277511', 'VariableUpdate', '277505', '277505', NULL, '277506', 'baseTask', 'serializable', 0, '2018-07-14 14:03:07.000', '277510', NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_detail` VALUES ('277516', 'VariableUpdate', '277505', '277505', NULL, '277512', 'flag', 'boolean', 0, '2018-07-14 14:04:51.000', NULL, NULL, 1, NULL, NULL);
INSERT INTO `act_hi_detail` VALUES ('277518', 'VariableUpdate', '277505', '277505', NULL, '277512', 'needfinish', 'integer', 0, '2018-07-14 14:04:51.000', NULL, NULL, 1, '1', NULL);
INSERT INTO `act_hi_detail` VALUES ('277523', 'VariableUpdate', '277505', '277505', NULL, '277512', 'leaveOpinionList', 'serializable', 0, '2018-07-14 14:04:51.000', '277522', NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_detail` VALUES ('277541', 'VariableUpdate', '277535', '277535', NULL, '277536', 'baseTask', 'serializable', 0, '2018-07-14 14:12:59.000', '277540', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for act_hi_identitylink
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_identitylink`;
CREATE TABLE `act_hi_identitylink`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `GROUP_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_IDENT_LNK_USER`(`USER_ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_IDENT_LNK_TASK`(`TASK_ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_IDENT_LNK_PROCINST`(`PROC_INST_ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_hi_identitylink
-- ----------------------------
INSERT INTO `act_hi_identitylink` VALUES ('277514', NULL, 'participant', 'd555ffd6b51f4df7a18e2ef4eece1bed', NULL, '277505');
INSERT INTO `act_hi_identitylink` VALUES ('277530', NULL, 'participant', '2211fec3e17c11e795ed201a068c6482', NULL, '277505');
INSERT INTO `act_hi_identitylink` VALUES ('277544', NULL, 'participant', 'd555ffd6b51f4df7a18e2ef4eece1bed', NULL, '277535');
INSERT INTO `act_hi_identitylink` VALUES ('277545', 'e346e96368484c8fa7f217ce550a0186', 'candidate', NULL, '277543', NULL);

-- ----------------------------
-- Table structure for act_hi_procinst
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_procinst`;
CREATE TABLE `act_hi_procinst`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `BUSINESS_KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `START_TIME_` datetime(3) NOT NULL,
  `END_TIME_` datetime(3) NULL DEFAULT NULL,
  `DURATION_` bigint(20) NULL DEFAULT NULL,
  `START_USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `START_ACT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `END_ACT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUPER_PROCESS_INSTANCE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DELETE_REASON_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  UNIQUE INDEX `PROC_INST_ID_`(`PROC_INST_ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_PRO_INST_END`(`END_TIME_`) USING BTREE,
  INDEX `ACT_IDX_HI_PRO_I_BUSKEY`(`BUSINESS_KEY_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_hi_procinst
-- ----------------------------
INSERT INTO `act_hi_procinst` VALUES ('247552', '247552', NULL, 'process_leave:1:247536', '2018-06-14 23:44:03.000', NULL, NULL, NULL, 'start', NULL, NULL, NULL, '', NULL);
INSERT INTO `act_hi_procinst` VALUES ('277505', '277505', NULL, 'process_leave:1:277504', '2018-07-14 14:03:07.000', NULL, NULL, NULL, 'start', NULL, NULL, NULL, '', NULL);
INSERT INTO `act_hi_procinst` VALUES ('277535', '277535', NULL, 'process_leave:2:277534', '2018-07-14 14:12:59.000', NULL, NULL, NULL, 'start', NULL, NULL, NULL, '', NULL);

-- ----------------------------
-- Table structure for act_hi_taskinst
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_taskinst`;
CREATE TABLE `act_hi_taskinst`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_DEF_KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PARENT_TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DESCRIPTION_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `OWNER_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ASSIGNEE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `START_TIME_` datetime(3) NOT NULL,
  `CLAIM_TIME_` datetime(3) NULL DEFAULT NULL,
  `END_TIME_` datetime(3) NULL DEFAULT NULL,
  `DURATION_` bigint(20) NULL DEFAULT NULL,
  `DELETE_REASON_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PRIORITY_` int(11) NULL DEFAULT NULL,
  `DUE_DATE_` datetime(3) NULL DEFAULT NULL,
  `FORM_KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_TASK_INST_PROCINST`(`PROC_INST_ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_hi_taskinst
-- ----------------------------
INSERT INTO `act_hi_taskinst` VALUES ('247560', 'process_leave:1:247536', 'user1', '247552', '247552', '经理审批', NULL, NULL, NULL, NULL, '2018-06-14 23:44:03.000', NULL, NULL, NULL, NULL, 50, NULL, NULL, NULL, '');
INSERT INTO `act_hi_taskinst` VALUES ('277513', 'process_leave:1:277504', 'user1', '277505', '277505', '经理审批', NULL, NULL, NULL, 'd555ffd6b51f4df7a18e2ef4eece1bed', '2018-07-14 14:03:07.000', NULL, '2018-07-14 14:04:51.000', 104513, 'completed', 50, NULL, NULL, NULL, '');
INSERT INTO `act_hi_taskinst` VALUES ('277529', 'process_leave:1:277504', 'allmanager', '277505', '277525', '总经理审批', NULL, NULL, NULL, '2211fec3e17c11e795ed201a068c6482', '2018-07-14 14:04:51.000', NULL, NULL, NULL, NULL, 50, NULL, NULL, NULL, '');
INSERT INTO `act_hi_taskinst` VALUES ('277543', 'process_leave:2:277534', 'user1', '277535', '277535', '经理审批', NULL, NULL, NULL, 'd555ffd6b51f4df7a18e2ef4eece1bed', '2018-07-14 14:12:59.000', NULL, NULL, NULL, NULL, 50, NULL, NULL, NULL, '');

-- ----------------------------
-- Table structure for act_hi_varinst
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_varinst`;
CREATE TABLE `act_hi_varinst`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VAR_TYPE_` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `BYTEARRAY_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DOUBLE_` double NULL DEFAULT NULL,
  `LONG_` bigint(20) NULL DEFAULT NULL,
  `TEXT_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TEXT2_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATE_TIME_` datetime(3) NULL DEFAULT NULL,
  `LAST_UPDATED_TIME_` datetime(3) NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_PROCVAR_PROC_INST`(`PROC_INST_ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_PROCVAR_NAME_TYPE`(`NAME_`, `VAR_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_HI_PROCVAR_TASK_ID`(`TASK_ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_hi_varinst
-- ----------------------------
INSERT INTO `act_hi_varinst` VALUES ('247555', '247552', '247552', NULL, 'userLeave', 'serializable', 0, '247556', NULL, NULL, NULL, NULL, '2018-06-14 23:44:03.000', '2018-06-14 23:44:03.000');
INSERT INTO `act_hi_varinst` VALUES ('277508', '277505', '277505', NULL, 'baseTask', 'serializable', 0, '277509', NULL, NULL, NULL, NULL, '2018-07-14 14:03:07.000', '2018-07-14 14:03:07.000');
INSERT INTO `act_hi_varinst` VALUES ('277515', '277505', '277505', NULL, 'flag', 'boolean', 0, NULL, NULL, 1, NULL, NULL, '2018-07-14 14:04:51.000', '2018-07-14 14:04:51.000');
INSERT INTO `act_hi_varinst` VALUES ('277517', '277505', '277505', NULL, 'needfinish', 'integer', 0, NULL, NULL, 1, '1', NULL, '2018-07-14 14:04:51.000', '2018-07-14 14:04:51.000');
INSERT INTO `act_hi_varinst` VALUES ('277520', '277505', '277505', NULL, 'leaveOpinionList', 'serializable', 0, '277521', NULL, NULL, NULL, NULL, '2018-07-14 14:04:51.000', '2018-07-14 14:04:51.000');
INSERT INTO `act_hi_varinst` VALUES ('277538', '277535', '277535', NULL, 'baseTask', 'serializable', 0, '277539', NULL, NULL, NULL, NULL, '2018-07-14 14:12:59.000', '2018-07-14 14:12:59.000');

-- ----------------------------
-- Table structure for act_id_group
-- ----------------------------
DROP TABLE IF EXISTS `act_id_group`;
CREATE TABLE `act_id_group`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_id_group
-- ----------------------------
INSERT INTO `act_id_group` VALUES ('023366f3457511e8bcf1309c2315f9aa', 1, 'hr', NULL);
INSERT INTO `act_id_group` VALUES ('0ea934e5e55411e7b983201a068c6482', 1, 'manage', NULL);
INSERT INTO `act_id_group` VALUES ('2619a672e53811e7b983201a068c6482', 1, 'admin', NULL);
INSERT INTO `act_id_group` VALUES ('4bb891d8caf84cc6ba27e515e80ac40d', 1, 'blogAdmin', NULL);
INSERT INTO `act_id_group` VALUES ('dcb0f642fe9611e7b472201a068c6482', 1, 'dev', NULL);
INSERT INTO `act_id_group` VALUES ('e346e96368484c8fa7f217ce550a0186', 1, 'DeputyManager', NULL);

-- ----------------------------
-- Table structure for act_id_info
-- ----------------------------
DROP TABLE IF EXISTS `act_id_info`;
CREATE TABLE `act_id_info`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `USER_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TYPE_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `VALUE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PASSWORD_` longblob NULL,
  `PARENT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_id_membership
-- ----------------------------
DROP TABLE IF EXISTS `act_id_membership`;
CREATE TABLE `act_id_membership`  (
  `USER_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `GROUP_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`USER_ID_`, `GROUP_ID_`) USING BTREE,
  INDEX `ACT_FK_MEMB_GROUP`(`GROUP_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_MEMB_GROUP` FOREIGN KEY (`GROUP_ID_`) REFERENCES `act_id_group` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_MEMB_USER` FOREIGN KEY (`USER_ID_`) REFERENCES `act_id_user` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_id_membership
-- ----------------------------
INSERT INTO `act_id_membership` VALUES ('2211fec3e17c11e795ed201a068c6482', '0ea934e5e55411e7b983201a068c6482');
INSERT INTO `act_id_membership` VALUES ('1ec421975ffe45229b48d4b9d712ff4f', 'dcb0f642fe9611e7b472201a068c6482');
INSERT INTO `act_id_membership` VALUES ('3c14c2f8316741e9aaeb29d78d03e958', 'dcb0f642fe9611e7b472201a068c6482');
INSERT INTO `act_id_membership` VALUES ('a4a743bffe9711e7b472201a068c6482', 'dcb0f642fe9611e7b472201a068c6482');

-- ----------------------------
-- Table structure for act_id_user
-- ----------------------------
DROP TABLE IF EXISTS `act_id_user`;
CREATE TABLE `act_id_user`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `FIRST_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `LAST_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EMAIL_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PWD_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PICTURE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_id_user
-- ----------------------------
INSERT INTO `act_id_user` VALUES ('1ec421975ffe45229b48d4b9d712ff4f', 1, '33', NULL, '', NULL, NULL);
INSERT INTO `act_id_user` VALUES ('2211fec3e17c11e795ed201a068c6482', 1, 'Tom Curise', NULL, '154040976@qq.com', NULL, NULL);
INSERT INTO `act_id_user` VALUES ('3c14c2f8316741e9aaeb29d78d03e958', 1, '222', NULL, '', NULL, NULL);
INSERT INTO `act_id_user` VALUES ('a4a743bffe9711e7b472201a068c6482', 1, 'zxm', NULL, '1544040976@qq.com', NULL, NULL);
INSERT INTO `act_id_user` VALUES ('acfc0e9232f54732a5d9ffe9071bf572', 1, '管理员', NULL, '', NULL, NULL);
INSERT INTO `act_id_user` VALUES ('d555ffd6b51f4df7a18e2ef4eece1bed', 1, '王五', NULL, '', NULL, NULL);
INSERT INTO `act_id_user` VALUES ('fb483b76457811e8bcf1309c2315f9aa', 1, '李四', NULL, '', NULL, NULL);

-- ----------------------------
-- Table structure for act_re_deployment
-- ----------------------------
DROP TABLE IF EXISTS `act_re_deployment`;
CREATE TABLE `act_re_deployment`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  `DEPLOY_TIME_` timestamp(3) NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_re_deployment
-- ----------------------------
INSERT INTO `act_re_deployment` VALUES ('247533', '请假流程', NULL, '', '2018-06-14 23:37:51.000');
INSERT INTO `act_re_deployment` VALUES ('277501', '请假流程', NULL, '', '2018-07-14 14:02:47.000');
INSERT INTO `act_re_deployment` VALUES ('277531', '请假流程', NULL, '', '2018-07-14 14:12:13.000');

-- ----------------------------
-- Table structure for act_re_procdef
-- ----------------------------
DROP TABLE IF EXISTS `act_re_procdef`;
CREATE TABLE `act_re_procdef`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VERSION_` int(11) NOT NULL,
  `DEPLOYMENT_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `RESOURCE_NAME_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DGRM_RESOURCE_NAME_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DESCRIPTION_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HAS_START_FORM_KEY_` tinyint(4) NULL DEFAULT NULL,
  `HAS_GRAPHICAL_NOTATION_` tinyint(4) NULL DEFAULT NULL,
  `SUSPENSION_STATE_` int(11) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  UNIQUE INDEX `ACT_UNIQ_PROCDEF`(`KEY_`, `VERSION_`, `TENANT_ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_re_procdef
-- ----------------------------
INSERT INTO `act_re_procdef` VALUES ('process_leave:1:277504', 1, 'http://www.activiti.org/processdef', '请假流程', 'process_leave', 1, '277501', '请假流程.bpmn20.xml', '请假流程.process_leave.png', NULL, 0, 1, 1, '');
INSERT INTO `act_re_procdef` VALUES ('process_leave:2:277534', 1, 'http://www.activiti.org/processdef', '请假流程', 'process_leave', 2, '277531', '请假流程.bpmn20.xml', '请假流程.process_leave.png', NULL, 0, 1, 1, '');

-- ----------------------------
-- Table structure for act_ru_execution
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_execution`;
CREATE TABLE `act_ru_execution`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `BUSINESS_KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PARENT_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUPER_EXEC_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ACT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `IS_ACTIVE_` tinyint(4) NULL DEFAULT NULL,
  `IS_CONCURRENT_` tinyint(4) NULL DEFAULT NULL,
  `IS_SCOPE_` tinyint(4) NULL DEFAULT NULL,
  `IS_EVENT_SCOPE_` tinyint(4) NULL DEFAULT NULL,
  `SUSPENSION_STATE_` int(11) NULL DEFAULT NULL,
  `CACHED_ENT_STATE_` int(11) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `LOCK_TIME_` timestamp(3) NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_EXEC_BUSKEY`(`BUSINESS_KEY_`) USING BTREE,
  INDEX `ACT_FK_EXE_PROCINST`(`PROC_INST_ID_`) USING BTREE,
  INDEX `ACT_FK_EXE_PARENT`(`PARENT_ID_`) USING BTREE,
  INDEX `ACT_FK_EXE_SUPER`(`SUPER_EXEC_`) USING BTREE,
  INDEX `ACT_FK_EXE_PROCDEF`(`PROC_DEF_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_EXE_PARENT` FOREIGN KEY (`PARENT_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_EXE_PROCDEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_EXE_PROCINST` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ACT_FK_EXE_SUPER` FOREIGN KEY (`SUPER_EXEC_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_ru_execution
-- ----------------------------
INSERT INTO `act_ru_execution` VALUES ('277505', 2, '277505', NULL, NULL, 'process_leave:1:277504', NULL, NULL, 0, 0, 1, 0, 1, 0, '', NULL, NULL);
INSERT INTO `act_ru_execution` VALUES ('277525', 1, '277505', NULL, '277505', 'process_leave:1:277504', NULL, 'allmanager', 1, 0, 1, 0, 1, 2, '', NULL, NULL);
INSERT INTO `act_ru_execution` VALUES ('277535', 1, '277535', NULL, NULL, 'process_leave:2:277534', NULL, 'user1', 1, 0, 1, 0, 1, 2, '', NULL, NULL);

-- ----------------------------
-- Table structure for act_ru_task
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_task`;
CREATE TABLE `act_ru_task`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PARENT_TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DESCRIPTION_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_DEF_KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `OWNER_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ASSIGNEE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DELEGATION_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PRIORITY_` int(11) NULL DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `DUE_DATE_` datetime(3) NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUSPENSION_STATE_` int(11) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  `FORM_KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_TASK_CREATE`(`CREATE_TIME_`) USING BTREE,
  INDEX `ACT_FK_TASK_EXE`(`EXECUTION_ID_`) USING BTREE,
  INDEX `ACT_FK_TASK_PROCINST`(`PROC_INST_ID_`) USING BTREE,
  INDEX `ACT_FK_TASK_PROCDEF`(`PROC_DEF_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_TASK_EXE` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_TASK_PROCDEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_TASK_PROCINST` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_ru_task
-- ----------------------------
INSERT INTO `act_ru_task` VALUES ('277529', 1, '277525', '277505', 'process_leave:1:277504', '总经理审批', NULL, NULL, 'allmanager', NULL, '2211fec3e17c11e795ed201a068c6482', NULL, 50, '2018-07-14 14:04:51.000', NULL, NULL, 1, '', NULL);
INSERT INTO `act_ru_task` VALUES ('277543', 1, '277535', '277535', 'process_leave:2:277534', '经理审批', NULL, NULL, 'user1', NULL, 'd555ffd6b51f4df7a18e2ef4eece1bed', NULL, 50, '2018-07-14 14:12:59.000', NULL, NULL, 1, '', NULL);

-- ----------------------------
-- Table structure for sys_dict_item
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_item`;
CREATE TABLE `sys_dict_item`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sequence` int(11) NOT NULL COMMENT '序号',
  `value` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '值',
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '描述',
  `type_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典id外检',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_date` datetime(0) NULL DEFAULT NULL,
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_date` datetime(0) NULL DEFAULT NULL,
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '删除标识',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典子表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编码',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '删除标识',
  `text` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典名称',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_date` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_date` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典主表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `job_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '描述任务',
  `cron` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务表达式',
  `status` tinyint(1) NOT NULL COMMENT '状态:0未启动false/1启动true',
  `clazz_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务执行方法',
  `job_desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '其他描述',
  `create_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_date` datetime(0) NULL DEFAULT NULL,
  `update_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_date` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES ('55147ebdf2f611e7a4fe201a068c6482', '测试定时demo1', '0/5 * * * * ?', 0, 'com.len.core.quartz.CustomQuartz.JobDemo1', '测试定时demo1', 'acfc0e9232f54732a5d9ffe9071bf572', '2018-01-07 12:30:00', 'acfc0e9232f54732a5d9ffe9071bf572', '2018-07-14 13:29:55');
INSERT INTO `sys_job` VALUES ('ab648a22f38d11e7aca0201a068c6482', '任务demo2', '0 0/1 * * * ?', 0, 'com.len.core.quartz.CustomQuartz.JobDemo2', '任务demo2', 'acfc0e9232f54732a5d9ffe9071bf572', '2018-01-07 17:32:36', 'acfc0e9232f54732a5d9ffe9071bf572', '2018-07-07 17:07:45');

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `text` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `param` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 38 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_log
-- ----------------------------
INSERT INTO `sys_log` VALUES (1, 'admin', '0:0:0:0:0:0:0:1', 'ATHOR', '添加用户', '[参数1:{\"age\":222,\"createBy\":\"acfc0e9232f54732a5d9ffe9071bf572\",\"createDate\":1534687227837,\"email\":\"\",\"id\":\"3c14c2f8316741e9aaeb29d78d03e958\",\"password\":\"2b8aae82f069cc838dfe8afc945f8045\",\"photo\":\"\",\"realName\":\"222\",\"username\":\"22222\"}][参数2:[\"dcb0f642fe9611e7b472201a068c6482\"]]', '2018-08-19 22:00:27');
INSERT INTO `sys_log` VALUES (2, 'admin', '0:0:0:0:0:0:0:1', 'ATHOR', '更新角色', '[参数1:{\"id\":\"e346e96368484c8fa7f217ce550a0186\",\"remark\":\"副经理\",\"roleName\":\"DeputyManager\"}][参数2:[\"e06da471f90311e780aa201a068c6482\",\"4d603831fe9b11e7b472201a068c6482\",\"b7839f59fe8811e7b472201a068c6482\"]]', '2018-08-19 22:37:10');
INSERT INTO `sys_log` VALUES (3, 'admin', '0:0:0:0:0:0:0:1', 'ATHOR', '添加用户', '[参数1:{\"age\":26,\"createBy\":\"acfc0e9232f54732a5d9ffe9071bf572\",\"createDate\":1534780367021,\"email\":\"\",\"id\":\"1ec421975ffe45229b48d4b9d712ff4f\",\"password\":\"533add1dc96c02469d50ca0ffdcb493a\",\"photo\":\"\",\"realName\":\"33\",\"username\":\"33333\"}][参数2:[\"dcb0f642fe9611e7b472201a068c6482\"]]', '2018-08-20 23:52:47');
INSERT INTO `sys_log` VALUES (4, 'admin', '0:0:0:0:0:0:0:1', 'DEL', '删除用户', '[参数1:\"7c54823822f74db38076391b815e7f32\"][参数2:false]', '2018-09-09 10:45:05');
INSERT INTO `sys_log` VALUES (5, 'admin', '0:0:0:0:0:0:0:1', 'DEL', '删除用户', '[参数1:\"7c9c4337873e4ffbb57052d6a852f78e\"][参数2:false]', '2018-09-09 10:45:08');
INSERT INTO `sys_log` VALUES (6, 'admin', '0:0:0:0:0:0:0:1', 'DEL', '删除用户', '[参数1:\"95ba3ecf3f53478e8adc6f2b31b92636\"][参数2:false]', '2018-09-09 10:45:11');
INSERT INTO `sys_log` VALUES (7, 'admin', '0:0:0:0:0:0:0:1', 'DEL', '删除用户', '[参数1:\"9e3dc630d2064a0084c8f43517f91520\"][参数2:false]', '2018-09-09 10:45:13');
INSERT INTO `sys_log` VALUES (8, 'admin', '0:0:0:0:0:0:0:1', 'DEL', '删除用户', '[参数1:\"b74ffd759ff34222939bfafe33886461\"][参数2:false]', '2018-09-09 10:45:15');
INSERT INTO `sys_log` VALUES (9, 'admin', '0:0:0:0:0:0:0:1', 'DEL', '删除用户', '[参数1:\"af36a0e897ab4a26b104b77427994e56\"][参数2:false]', '2018-09-09 10:45:17');
INSERT INTO `sys_log` VALUES (10, 'admin', '0:0:0:0:0:0:0:1', 'DEL', '删除用户', '[参数1:\"c28be4153d1c4df7b955aa88b1f0cf9a\"][参数2:false]', '2018-09-09 10:45:19');
INSERT INTO `sys_log` VALUES (11, 'admin', '0:0:0:0:0:0:0:1', 'DEL', '删除用户', '[参数1:\"eaca1d4854ba44228cc4c0ed38546b17\"][参数2:false]', '2018-09-09 10:45:21');
INSERT INTO `sys_log` VALUES (12, 'admin', '0:0:0:0:0:0:0:1', 'DEL', '删除用户', '[参数1:\"0f3672f9dcb2408c87be10d9f1702c03\"][参数2:false]', '2018-09-09 10:45:23');
INSERT INTO `sys_log` VALUES (13, 'admin', '0:0:0:0:0:0:0:1', 'DEL', '删除用户', '[参数1:\"237b2cf56a3248f9a43a4f2194ac3317\"][参数2:false]', '2018-09-09 10:45:25');
INSERT INTO `sys_log` VALUES (14, 'admin', '0:0:0:0:0:0:0:1', 'DEL', '删除用户', '[参数1:\"2453ed0f16e6402b8acecd6b6dfac789\"][参数2:false]', '2018-09-09 10:45:27');
INSERT INTO `sys_log` VALUES (15, 'admin', '0:0:0:0:0:0:0:1', 'ATHOR', '添加角色', '[参数1:{\"createBy\":\"acfc0e9232f54732a5d9ffe9071bf572\",\"createDate\":1538360470622,\"id\":\"4bb891d8caf84cc6ba27e515e80ac40d\",\"remark\":\"博客管理员\",\"roleName\":\"blogAdmin\"}][参数2:null]', '2018-10-01 10:21:10');
INSERT INTO `sys_log` VALUES (16, 'admin', '0:0:0:0:0:0:0:1', 'UPDATE', '更新用户', '[参数1:{\"age\":24,\"email\":\"\",\"id\":\"acfc0e9232f54732a5d9ffe9071bf572\",\"photo\":\"2d4c37c3-c106-4288-9c0d-e7fe1b8adc72.jpeg\",\"realName\":\"管理员\"}][参数2:[\"4bb891d8caf84cc6ba27e515e80ac40d\",\"2619a672e53811e7b983201a068c6482\"]]', '2018-10-01 10:21:19');
INSERT INTO `sys_log` VALUES (17, 'admin', '0:0:0:0:0:0:0:1', 'ATHOR', '更新角色', '[参数1:{\"id\":\"4bb891d8caf84cc6ba27e515e80ac40d\",\"remark\":\"博客管理员\",\"roleName\":\"blogAdmin\"}][参数2:null]', '2018-10-12 22:14:22');
INSERT INTO `sys_log` VALUES (18, 'admin', '0:0:0:0:0:0:0:1', 'UPDATE', '更新用户', '[参数1:{\"age\":25,\"email\":\"1544040976@qq.com\",\"id\":\"a4a743bffe9711e7b472201a068c6482\",\"photo\":\"662d5a3b-56aa-4bbb-bd47-194e24db1d60.jpeg\",\"realName\":\"zxm\"}][参数2:[\"4bb891d8caf84cc6ba27e515e80ac40d\"]]', '2018-11-25 15:00:41');
INSERT INTO `sys_log` VALUES (19, 'admin', '0:0:0:0:0:0:0:1', 'UPDATE', '修改密码', '[参数1:\"a4a743bffe9711e7b472201a068c6482\"][参数2:\"123456\"][参数3:\"123456\"]', '2018-11-25 15:01:03');
INSERT INTO `sys_log` VALUES (26, 'admin', '0:0:0:0:0:0:0:1', 'ATHOR', '添加角色', '[参数1:{\"createBy\":\"acfc0e9232f54732a5d9ffe9071bf572\",\"createDate\":1544529759053,\"id\":\"35f7934d335e4641b7887e4b3b1885a5\",\"remark\":\"cs\",\"roleName\":\"cs\"}][参数2:[\"cfda8029dfb311e7b555201a068c6482\",\"3873ccc2dfda11e7b555201a068c6482\",\"18bf8d5df09511e78a57201a068c6482\"]]', '2018-12-11 20:02:39');
INSERT INTO `sys_log` VALUES (27, 'admin', '0:0:0:0:0:0:0:1', 'DEL', '删除角色', '[参数1:\"35f7934d335e4641b7887e4b3b1885a5\"]', '2018-12-11 20:02:46');
INSERT INTO `sys_log` VALUES (28, 'admin', '0:0:0:0:0:0:0:1', 'UPDATE', '添加菜单', '[参数1:{\"children\":[],\"createBy\":\"acfc0e9232f54732a5d9ffe9071bf572\",\"createDate\":1544538738063,\"icon\":\"\",\"id\":\"3fa6f0049d774882a91b176415fc56a8\",\"menuType\":0,\"name\":\"11\",\"num\":0,\"orderNum\":1,\"url\":\"11\"}][参数2:', '2018-12-11 22:32:18');
INSERT INTO `sys_log` VALUES (29, 'admin', '0:0:0:0:0:0:0:1', 'ADD', '更新菜单', '[参数1:{\"children\":[],\"icon\":\"\",\"id\":\"3fa6f0049d774882a91b176415fc56a8\",\"name\":\"11\",\"num\":0,\"orderNum\":1,\"permission\":\"\",\"url\":\"11\"}]', '2018-12-11 22:32:22');
INSERT INTO `sys_log` VALUES (30, 'admin', '0:0:0:0:0:0:0:1', 'DEL', '删除菜单', '[参数1:\"3fa6f0049d774882a91b176415fc56a8\"]', '2018-12-11 22:32:25');
INSERT INTO `sys_log` VALUES (31, 'admin', '0:0:0:0:0:0:0:1', 'ATHOR', '添加用户', '[参数1:{\"age\":111,\"createBy\":\"acfc0e9232f54732a5d9ffe9071bf572\",\"createDate\":1544538762207,\"email\":\"\",\"id\":\"a07e4c55015147789d5e1e797b4af2ec\",\"password\":\"0a27dab34aa0db28fb5d04f62989cb04\",\"photo\":\"\",\"realName\":\"1111\",\"username\":\"1111\"}][参数2:[\"dcb0f642fe9611e7b472201a068c6482\"]]', '2018-12-11 22:32:42');
INSERT INTO `sys_log` VALUES (32, 'admin', '0:0:0:0:0:0:0:1', 'UPDATE', '更新用户', '[参数1:{\"age\":111,\"email\":\"\",\"id\":\"a07e4c55015147789d5e1e797b4af2ec\",\"photo\":\"\",\"realName\":\"1111\"}][参数2:[\"dcb0f642fe9611e7b472201a068c6482\"]]', '2018-12-11 22:32:51');
INSERT INTO `sys_log` VALUES (33, 'admin', '0:0:0:0:0:0:0:1', 'DEL', '删除用户', '[参数1:\"a07e4c55015147789d5e1e797b4af2ec\"][参数2:false]', '2018-12-11 22:32:57');
INSERT INTO `sys_log` VALUES (34, 'admin', '0:0:0:0:0:0:0:1', 'UPDATE', '更新用户', '[参数1:{\"age\":111,\"email\":\"\",\"id\":\"a07e4c55015147789d5e1e797b4af2ec\",\"photo\":\"\",\"realName\":\"1111\"}][参数2:null]', '2018-12-11 22:33:04');
INSERT INTO `sys_log` VALUES (35, 'admin', '0:0:0:0:0:0:0:1', 'DEL', '删除用户', '[参数1:\"a07e4c55015147789d5e1e797b4af2ec\"][参数2:false]', '2018-12-11 22:33:07');
INSERT INTO `sys_log` VALUES (36, 'admin', '0:0:0:0:0:0:0:1', 'ATHOR', '更新角色', '[参数1:{\"id\":\"2619a672e53811e7b983201a068c6482\",\"remark\":\"管理员\",\"roleName\":\"admin\"}][参数2:[\"cfda8029dfb311e7b555201a068c6482\",\"3873ccc2dfda11e7b555201a068c6482\",\"18bf8d5df09511e78a57201a068c6482\",\"cfe54921dfb311e7b555201a068c6482\",\"433089a6eb0111e782d5201a068c6482\",\"cfe54921dfb311e7b555201a068c6483\",\"e3b11497eb9e11e7928d201a068c6482\",\"f23f6a6bf09511e78a57201a068c6482\",\"cff61424dfb311e7b555201a068c6482\",\"0e6c8d4cf09511e78a57201a068c6482\",\"2b56410cf09411e78a57201a068c6482\",\"88b8e5d1f38911e7aca0201a068c6482\",\"ff015ea5f09411e78a57201a068c6482\",\"a1ca6642ec5e11e7a472201a068c6482\",\"6dc13c6eec5f11e7a472201a068c6482\",\"7967e098ee0611e7a60d201a068c6482\",\"b441914cee0811e7a60d201a068c6482\",\"6931fd22f09611e78a57201a068c6482\",\"e9a13e55f35911e7aca0201a068c6482\",\"5ae3d4e9f38e11e7aca0201a068c6482\",\"6315968bf37111e7aca0201a068c6482\",\"69f3f59cf38e11e7aca0201a068c6482\",\"788d8e34f38e11e7aca0201a068c6482\",\"873f30b0f38e11e7aca0201a068c6482\",\"ecda560cf36f11e7aca0201a068c6482\",\"e06da471f90311e780aa201a068c6482\",\"28661300f9d411e7a009201a068c6482\",\"4d603831fe9b11e7b472201a068c6482\",\"63da4415fc6211e7a781201a068c6482\",\"b7839f59fe8811e7b472201a068c6482\"]]', '2018-12-11 22:33:30');
INSERT INTO `sys_log` VALUES (37, 'admin', '0:0:0:0:0:0:0:1', 'UPDATE', '更新用户', '[参数1:{\"age\":26,\"email\":\"\",\"id\":\"1ec421975ffe45229b48d4b9d712ff4f\",\"photo\":\"\",\"realName\":\"33\"}][参数2:[\"dcb0f642fe9611e7b472201a068c6482\"]]', '2018-12-11 22:57:16');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `p_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `order_num` int(4) NULL DEFAULT NULL COMMENT '排序字段',
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_date` datetime(0) NULL DEFAULT NULL,
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_date` datetime(0) NULL DEFAULT NULL,
  `permission` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限',
  `menu_type` tinyint(4) NOT NULL DEFAULT 0 COMMENT '1栏目2菜单',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('0e6c8d4cf09511e78a57201a068c6482', '删除', 'cff61424dfb311e7b555201a068c6482', NULL, 3, '', 'acfc0e9232f54732a5d9ffe9071bf572', '2018-01-03 22:47:44', NULL, NULL, 'role:del', 1);
INSERT INTO `sys_menu` VALUES ('18bf8d5df09511e78a57201a068c6482', '新增', '3873ccc2dfda11e7b555201a068c6482', NULL, 1, '', 'acfc0e9232f54732a5d9ffe9071bf572', '2018-01-03 22:48:01', NULL, NULL, 'nemu:add', 1);
INSERT INTO `sys_menu` VALUES ('28661300f9d411e7a009201a068c6482', '流程管理', 'e06da471f90311e780aa201a068c6482', '/act/goAct', 2, '&#xe630;', 'acfc0e9232f54732a5d9ffe9071bf572', '2018-01-15 17:12:06', NULL, NULL, 'act:deployment', 0);
INSERT INTO `sys_menu` VALUES ('2b56410cf09411e78a57201a068c6482', '新增', 'cff61424dfb311e7b555201a068c6482', NULL, 1, '', 'acfc0e9232f54732a5d9ffe9071bf572', '2018-01-03 22:41:23', NULL, NULL, 'role:add', 1);
INSERT INTO `sys_menu` VALUES ('3873ccc2dfda11e7b555201a068c6482', '菜单管理', 'cfda8029dfb311e7b555201a068c6482', 'menu/showMenu', 1, '', NULL, '2017-12-14 14:02:50', 'acfc0e9232f54732a5d9ffe9071bf572', '2018-04-23 19:43:54', 'menu:show', 0);
INSERT INTO `sys_menu` VALUES ('433089a6eb0111e782d5201a068c6482', '编辑', 'cfe54921dfb311e7b555201a068c6482', '', NULL, '1', 'acfc0e9232f54732a5d9ffe9071bf572', '2017-12-27 20:27:11', NULL, NULL, 'user:update', 1);
INSERT INTO `sys_menu` VALUES ('4d603831fe9b11e7b472201a068c6482', '待办任务', 'e06da471f90311e780aa201a068c6482', '/leave/showTask', 5, '&#xe6af;', 'acfc0e9232f54732a5d9ffe9071bf572', '2018-01-21 19:07:43', NULL, NULL, 'task:show', 0);
INSERT INTO `sys_menu` VALUES ('5ae3d4e9f38e11e7aca0201a068c6482', '新增', 'e9a13e55f35911e7aca0201a068c6482', NULL, 1, '', 'acfc0e9232f54732a5d9ffe9071bf572', '2018-01-07 17:37:30', NULL, NULL, 'job:add', 1);
INSERT INTO `sys_menu` VALUES ('6315968bf37111e7aca0201a068c6482', '停止', 'e9a13e55f35911e7aca0201a068c6482', NULL, 4, '', 'acfc0e9232f54732a5d9ffe9071bf572', '2018-01-07 14:10:09', NULL, NULL, 'job:end', 1);
INSERT INTO `sys_menu` VALUES ('63da4415fc6211e7a781201a068c6482', '模型列表', 'e06da471f90311e780aa201a068c6482', '/act/goActModel', 3, '&#xe60a;', 'acfc0e9232f54732a5d9ffe9071bf572', '2018-01-18 23:15:17', NULL, NULL, 'act', 0);
INSERT INTO `sys_menu` VALUES ('6931fd22f09611e78a57201a068c6482', '删除', 'b441914cee0811e7a60d201a068c6482', NULL, 1, '', 'acfc0e9232f54732a5d9ffe9071bf572', '2018-01-03 22:57:26', NULL, NULL, 'control:del', 1);
INSERT INTO `sys_menu` VALUES ('69f3f59cf38e11e7aca0201a068c6482', '编辑', 'e9a13e55f35911e7aca0201a068c6482', NULL, 2, '', 'acfc0e9232f54732a5d9ffe9071bf572', '2018-01-07 17:37:56', NULL, NULL, 'job:update', 1);
INSERT INTO `sys_menu` VALUES ('6dc13c6eec5f11e7a472201a068c6482', '系统日志', 'a1ca6642ec5e11e7a472201a068c6482', 'log/showLog', 1, '&#xe60a;', 'acfc0e9232f54732a5d9ffe9071bf572', '2017-12-29 14:43:46', NULL, '2017-12-29 14:43:46', 'log:show', 0);
INSERT INTO `sys_menu` VALUES ('788d8e34f38e11e7aca0201a068c6482', '删除', 'e9a13e55f35911e7aca0201a068c6482', NULL, 5, '', 'acfc0e9232f54732a5d9ffe9071bf572', '2018-01-07 17:38:20', NULL, NULL, 'job:del', 1);
INSERT INTO `sys_menu` VALUES ('7967e098ee0611e7a60d201a068c6482', '接口api', 'a1ca6642ec5e11e7a472201a068c6482', 'swagger-ui.html', 2, '&#xe64e;', 'acfc0e9232f54732a5d9ffe9071bf572', '2017-12-31 16:42:04', NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES ('873f30b0f38e11e7aca0201a068c6482', '查看', 'e9a13e55f35911e7aca0201a068c6482', NULL, 6, '', 'acfc0e9232f54732a5d9ffe9071bf572', '2018-01-07 17:38:45', NULL, NULL, 'job:select', 1);
INSERT INTO `sys_menu` VALUES ('88b8e5d1f38911e7aca0201a068c6482', '查看', 'cff61424dfb311e7b555201a068c6482', NULL, 4, '', 'acfc0e9232f54732a5d9ffe9071bf572', '2018-01-07 17:03:00', NULL, NULL, 'role:select', 1);
INSERT INTO `sys_menu` VALUES ('8a6c8bfa7f804eac810c5790cad9a62a', '删除', '3873ccc2dfda11e7b555201a068c6482', NULL, 2, NULL, 'acfc0e9232f54732a5d9ffe9071bf572', '2018-06-20 21:55:55', NULL, NULL, 'menu:del', 1);
INSERT INTO `sys_menu` VALUES ('a1ca6642ec5e11e7a472201a068c6482', '系统监控', NULL, NULL, 2, '&#xe62c;', 'acfc0e9232f54732a5d9ffe9071bf572', '2018-01-03 10:56:14', NULL, '2018-01-03 10:56:14', NULL, 0);
INSERT INTO `sys_menu` VALUES ('b441914cee0811e7a60d201a068c6482', '系统监控', 'a1ca6642ec5e11e7a472201a068c6482', 'druid/index.html', 3, '&#xe628;', 'acfc0e9232f54732a5d9ffe9071bf572', '2017-12-31 16:58:01', NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES ('b7839f59fe8811e7b472201a068c6482', '请假流程', 'e06da471f90311e780aa201a068c6482', '/leave/showLeave', 4, '&#xe650;', 'acfc0e9232f54732a5d9ffe9071bf572', '2018-01-21 16:54:40', NULL, NULL, 'leave:show', 0);
INSERT INTO `sys_menu` VALUES ('cfda8029dfb311e7b555201a068c6482', '系统管理', NULL, NULL, 1, '&#xe614;', NULL, '2018-01-03 10:56:13', NULL, '2018-01-03 10:56:13', NULL, 0);
INSERT INTO `sys_menu` VALUES ('cfe54921dfb311e7b555201a068c6482', '用户管理', 'cfda8029dfb311e7b555201a068c6482', '/user/showUser', 2, '&#xe6af;', NULL, '2017-12-29 14:40:34', NULL, '2017-12-29 14:40:34', 'user:show', 0);
INSERT INTO `sys_menu` VALUES ('cfe54921dfb311e7b555201a068c6483', '增加', 'cfe54921dfb311e7b555201a068c6482', NULL, 1, NULL, NULL, NULL, NULL, NULL, 'user:select', 1);
INSERT INTO `sys_menu` VALUES ('cff61424dfb311e7b555201a068c6482', '角色管理', 'cfda8029dfb311e7b555201a068c6482', '/role/showRole', 3, '&#xe613;', NULL, '2017-12-29 14:40:36', NULL, '2017-12-29 14:40:36', 'role:show', 0);
INSERT INTO `sys_menu` VALUES ('e06da471f90311e780aa201a068c6482', '工作流程管理', NULL, NULL, 3, '&#xe628;', 'acfc0e9232f54732a5d9ffe9071bf572', '2018-01-14 16:21:10', NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES ('e3b11497eb9e11e7928d201a068c6482', '删除', 'cfe54921dfb311e7b555201a068c6482', '', NULL, '', 'acfc0e9232f54732a5d9ffe9071bf572', '2017-12-28 15:17:07', NULL, '2017-12-28 15:17:07', 'user:del', 1);
INSERT INTO `sys_menu` VALUES ('e9a13e55f35911e7aca0201a068c6482', '定时任务', 'a1ca6642ec5e11e7a472201a068c6482', '/job/showJob', 3, '&#xe756;', 'acfc0e9232f54732a5d9ffe9071bf572', '2018-01-07 11:22:06', NULL, NULL, 'job:show', 0);
INSERT INTO `sys_menu` VALUES ('ecda560cf36f11e7aca0201a068c6482', '启动', 'e9a13e55f35911e7aca0201a068c6482', NULL, 3, '', 'acfc0e9232f54732a5d9ffe9071bf572', '2018-01-07 13:59:41', NULL, NULL, 'job:start', 1);
INSERT INTO `sys_menu` VALUES ('f23f6a6bf09511e78a57201a068c6482', '修改密码', 'cfe54921dfb311e7b555201a068c6482', NULL, 4, '', 'acfc0e9232f54732a5d9ffe9071bf572', '2018-01-03 22:54:06', NULL, NULL, 'user:repass', 1);
INSERT INTO `sys_menu` VALUES ('ff015ea5f09411e78a57201a068c6482', '编辑', 'cff61424dfb311e7b555201a068c6482', NULL, 2, '', 'acfc0e9232f54732a5d9ffe9071bf572', '2018-01-03 22:47:18', NULL, NULL, 'role:update', 1);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `role_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_date` datetime(0) NULL DEFAULT NULL,
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_date` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('023366f3457511e8bcf1309c2315f9aa', 'hr', '人事', 'acfc0e9232f54732a5d9ffe9071bf572', '2018-04-21 23:02:16', NULL, NULL);
INSERT INTO `sys_role` VALUES ('0ea934e5e55411e7b983201a068c6482', 'manage', '经理', 'acfc0e9232f54732a5d9ffe9071bf572', '2017-12-20 15:04:44', NULL, '2018-01-02 11:41:43');
INSERT INTO `sys_role` VALUES ('2619a672e53811e7b983201a068c6482', 'admin', '管理员', 'acfc0e9232f54732a5d9ffe9071bf572', '2017-12-20 11:44:57', NULL, '2018-01-02 11:38:37');
INSERT INTO `sys_role` VALUES ('4bb891d8caf84cc6ba27e515e80ac40d', 'blogAdmin', '博客管理员', 'acfc0e9232f54732a5d9ffe9071bf572', '2018-10-01 10:21:10', NULL, NULL);
INSERT INTO `sys_role` VALUES ('dcb0f642fe9611e7b472201a068c6482', 'dev', '开发', 'acfc0e9232f54732a5d9ffe9071bf572', '2018-01-21 18:35:56', NULL, NULL);
INSERT INTO `sys_role` VALUES ('e346e96368484c8fa7f217ce550a0186', 'DeputyManager', '副经理', 'acfc0e9232f54732a5d9ffe9071bf572', '2018-06-14 23:21:36', NULL, NULL);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `menu_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('023366f3457511e8bcf1309c2315f9aa', '433089a6eb0111e782d5201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('023366f3457511e8bcf1309c2315f9aa', '4d603831fe9b11e7b472201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('023366f3457511e8bcf1309c2315f9aa', 'b7839f59fe8811e7b472201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('023366f3457511e8bcf1309c2315f9aa', 'cfda8029dfb311e7b555201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('023366f3457511e8bcf1309c2315f9aa', 'cfe54921dfb311e7b555201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('023366f3457511e8bcf1309c2315f9aa', 'cfe54921dfb311e7b555201a068c6483');
INSERT INTO `sys_role_menu` VALUES ('023366f3457511e8bcf1309c2315f9aa', 'e06da471f90311e780aa201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('023366f3457511e8bcf1309c2315f9aa', 'e3b11497eb9e11e7928d201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('023366f3457511e8bcf1309c2315f9aa', 'f23f6a6bf09511e78a57201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('0ea934e5e55411e7b983201a068c6482', '433089a6eb0111e782d5201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('0ea934e5e55411e7b983201a068c6482', '4d603831fe9b11e7b472201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('0ea934e5e55411e7b983201a068c6482', '6931fd22f09611e78a57201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('0ea934e5e55411e7b983201a068c6482', '6dc13c6eec5f11e7a472201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('0ea934e5e55411e7b983201a068c6482', 'a1ca6642ec5e11e7a472201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('0ea934e5e55411e7b983201a068c6482', 'b7839f59fe8811e7b472201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('0ea934e5e55411e7b983201a068c6482', 'cfda8029dfb311e7b555201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('0ea934e5e55411e7b983201a068c6482', 'cfe54921dfb311e7b555201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('0ea934e5e55411e7b983201a068c6482', 'e06da471f90311e780aa201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('2619a672e53811e7b983201a068c6482', '0e6c8d4cf09511e78a57201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('2619a672e53811e7b983201a068c6482', '18bf8d5df09511e78a57201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('2619a672e53811e7b983201a068c6482', '28661300f9d411e7a009201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('2619a672e53811e7b983201a068c6482', '2b56410cf09411e78a57201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('2619a672e53811e7b983201a068c6482', '3873ccc2dfda11e7b555201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('2619a672e53811e7b983201a068c6482', '433089a6eb0111e782d5201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('2619a672e53811e7b983201a068c6482', '4d603831fe9b11e7b472201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('2619a672e53811e7b983201a068c6482', '5ae3d4e9f38e11e7aca0201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('2619a672e53811e7b983201a068c6482', '6315968bf37111e7aca0201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('2619a672e53811e7b983201a068c6482', '63da4415fc6211e7a781201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('2619a672e53811e7b983201a068c6482', '6931fd22f09611e78a57201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('2619a672e53811e7b983201a068c6482', '69f3f59cf38e11e7aca0201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('2619a672e53811e7b983201a068c6482', '6dc13c6eec5f11e7a472201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('2619a672e53811e7b983201a068c6482', '788d8e34f38e11e7aca0201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('2619a672e53811e7b983201a068c6482', '7967e098ee0611e7a60d201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('2619a672e53811e7b983201a068c6482', '873f30b0f38e11e7aca0201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('2619a672e53811e7b983201a068c6482', '88b8e5d1f38911e7aca0201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('2619a672e53811e7b983201a068c6482', 'a1ca6642ec5e11e7a472201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('2619a672e53811e7b983201a068c6482', 'b441914cee0811e7a60d201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('2619a672e53811e7b983201a068c6482', 'b7839f59fe8811e7b472201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('2619a672e53811e7b983201a068c6482', 'cfda8029dfb311e7b555201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('2619a672e53811e7b983201a068c6482', 'cfe54921dfb311e7b555201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('2619a672e53811e7b983201a068c6482', 'cfe54921dfb311e7b555201a068c6483');
INSERT INTO `sys_role_menu` VALUES ('2619a672e53811e7b983201a068c6482', 'cff61424dfb311e7b555201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('2619a672e53811e7b983201a068c6482', 'e06da471f90311e780aa201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('2619a672e53811e7b983201a068c6482', 'e3b11497eb9e11e7928d201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('2619a672e53811e7b983201a068c6482', 'e9a13e55f35911e7aca0201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('2619a672e53811e7b983201a068c6482', 'ecda560cf36f11e7aca0201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('2619a672e53811e7b983201a068c6482', 'f23f6a6bf09511e78a57201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('2619a672e53811e7b983201a068c6482', 'ff015ea5f09411e78a57201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('35f7934d335e4641b7887e4b3b1885a5', '18bf8d5df09511e78a57201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('35f7934d335e4641b7887e4b3b1885a5', '3873ccc2dfda11e7b555201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('35f7934d335e4641b7887e4b3b1885a5', 'cfda8029dfb311e7b555201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('a56219ffeb7d11e7928d201a068c6482', '433089a6eb0111e782d5201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('a56219ffeb7d11e7928d201a068c6482', 'cfda8029dfb311e7b555201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('a56219ffeb7d11e7928d201a068c6482', 'cfe54921dfb311e7b555201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('dcb0f642fe9611e7b472201a068c6482', '4d603831fe9b11e7b472201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('dcb0f642fe9611e7b472201a068c6482', '5ae3d4e9f38e11e7aca0201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('dcb0f642fe9611e7b472201a068c6482', '6315968bf37111e7aca0201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('dcb0f642fe9611e7b472201a068c6482', '69f3f59cf38e11e7aca0201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('dcb0f642fe9611e7b472201a068c6482', '788d8e34f38e11e7aca0201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('dcb0f642fe9611e7b472201a068c6482', '873f30b0f38e11e7aca0201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('dcb0f642fe9611e7b472201a068c6482', 'a1ca6642ec5e11e7a472201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('dcb0f642fe9611e7b472201a068c6482', 'b7839f59fe8811e7b472201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('dcb0f642fe9611e7b472201a068c6482', 'e06da471f90311e780aa201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('dcb0f642fe9611e7b472201a068c6482', 'e9a13e55f35911e7aca0201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('dcb0f642fe9611e7b472201a068c6482', 'ecda560cf36f11e7aca0201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('e346e96368484c8fa7f217ce550a0186', '4d603831fe9b11e7b472201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('e346e96368484c8fa7f217ce550a0186', 'b7839f59fe8811e7b472201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('e346e96368484c8fa7f217ce550a0186', 'e06da471f90311e780aa201a068c6482');

-- ----------------------------
-- Table structure for sys_role_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_user`;
CREATE TABLE `sys_role_user`  (
  `user_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `role_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_user
-- ----------------------------
INSERT INTO `sys_role_user` VALUES ('1ec421975ffe45229b48d4b9d712ff4f', 'dcb0f642fe9611e7b472201a068c6482');
INSERT INTO `sys_role_user` VALUES ('2211fec3e17c11e795ed201a068c6482', '0ea934e5e55411e7b983201a068c6482');
INSERT INTO `sys_role_user` VALUES ('3c14c2f8316741e9aaeb29d78d03e958', 'dcb0f642fe9611e7b472201a068c6482');
INSERT INTO `sys_role_user` VALUES ('a4a743bffe9711e7b472201a068c6482', '4bb891d8caf84cc6ba27e515e80ac40d');
INSERT INTO `sys_role_user` VALUES ('acfc0e9232f54732a5d9ffe9071bf572', '2619a672e53811e7b983201a068c6482');
INSERT INTO `sys_role_user` VALUES ('acfc0e9232f54732a5d9ffe9071bf572', '4bb891d8caf84cc6ba27e515e80ac40d');
INSERT INTO `sys_role_user` VALUES ('d555ffd6b51f4df7a18e2ef4eece1bed', 'e346e96368484c8fa7f217ce550a0186');
INSERT INTO `sys_role_user` VALUES ('fb483b76457811e8bcf1309c2315f9aa', '023366f3457511e8bcf1309c2315f9aa');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `username` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `age` int(4) NULL DEFAULT NULL,
  `email` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `photo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `real_name` varchar(18) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_date` datetime(0) NULL DEFAULT NULL,
  `update_date` datetime(0) NULL DEFAULT NULL,
  `del_flag` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0可用1封禁',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1ec421975ffe45229b48d4b9d712ff4f', '33333', '533add1dc96c02469d50ca0ffdcb493a', 26, NULL, NULL, '33', 'acfc0e9232f54732a5d9ffe9071bf572', 'acfc0e9232f54732a5d9ffe9071bf572', '2018-08-20 23:52:47', '2018-12-11 22:57:16', 0);
INSERT INTO `sys_user` VALUES ('2211fec3e17c11e795ed201a068c6482', 'tom', '11ac200620f90acd1fdae53716fd3de2', 41, '154040976@qq.com', 'bd214483-7c5e-49d6-862d-de97e9de50b5.jpeg', 'Tom Curise', NULL, 'acfc0e9232f54732a5d9ffe9071bf572', '2018-01-03 15:49:48', '2018-06-19 21:15:49', 0);
INSERT INTO `sys_user` VALUES ('2bf2d2db774247b99f27efb1dda29c34', '12', '123456', 10, '2345', NULL, '123', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_user` VALUES ('3c14c2f8316741e9aaeb29d78d03e958', '22222', '2b8aae82f069cc838dfe8afc945f8045', 222, NULL, NULL, '222', 'acfc0e9232f54732a5d9ffe9071bf572', NULL, '2018-08-19 22:00:27', NULL, 0);
INSERT INTO `sys_user` VALUES ('a4a743bffe9711e7b472201a068c6482', 'zxm', 'f8880ebbdbc37a936245657fa9084198', 25, '1544040976@qq.com', '662d5a3b-56aa-4bbb-bd47-194e24db1d60.jpeg', 'zxm', 'acfc0e9232f54732a5d9ffe9071bf572', 'acfc0e9232f54732a5d9ffe9071bf572', '2018-01-21 18:41:32', '2018-11-25 15:00:41', 0);
INSERT INTO `sys_user` VALUES ('acfc0e9232f54732a5d9ffe9071bf572', 'admin', 'e0b141de1c8091be350d3fc80de66528', 24, '', '2d4c37c3-c106-4288-9c0d-e7fe1b8adc72.jpeg', '管理员', NULL, 'acfc0e9232f54732a5d9ffe9071bf572', '2017-12-20 16:34:06', '2018-10-01 10:21:19', 0);
INSERT INTO `sys_user` VALUES ('b50d049022124b04b73605caae5ecb3b', '12', '123456', 10, '2345', NULL, '123', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_user` VALUES ('c7f1a7d7018311e8a1a2201a068c6482', '666', 'c6953f608430df414ea52e8c01b81a45', 24, '', '', '666', 'acfc0e9232f54732a5d9ffe9071bf572', 'acfc0e9232f54732a5d9ffe9071bf572', '2018-01-25 11:56:54', '2018-06-14 23:18:51', 1);
INSERT INTO `sys_user` VALUES ('d555ffd6b51f4df7a18e2ef4eece1bed', 'wangwu', 'b162011c014942eac61c478a7bfc386d', 25, '', '', '王五', 'acfc0e9232f54732a5d9ffe9071bf572', 'acfc0e9232f54732a5d9ffe9071bf572', '2018-06-14 23:21:17', '2018-06-19 23:09:12', 0);
INSERT INTO `sys_user` VALUES ('fb483b76457811e8bcf1309c2315f9aa', 'lisi', 'f497935e5f47325399d595ef31b25e47', 20, '', '', '李四', 'acfc0e9232f54732a5d9ffe9071bf572', 'acfc0e9232f54732a5d9ffe9071bf572', '2018-04-21 23:30:43', '2018-06-14 23:21:50', 0);
INSERT INTO `sys_user` VALUES ('fbb427b4e2764260a337f074744bc55a', '12', '123456', 10, '2345', NULL, '123', NULL, NULL, NULL, NULL, 0);

-- ----------------------------
-- Table structure for test
-- ----------------------------
DROP TABLE IF EXISTS `test`;
CREATE TABLE `test`  (
  `id` int(1) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of test
-- ----------------------------
INSERT INTO `test` VALUES (1, '李辉');
INSERT INTO `test` VALUES (2, '李辉2');
INSERT INTO `test` VALUES (3, '李辉3');
INSERT INTO `test` VALUES (4, '李辉4');
INSERT INTO `test` VALUES (5, '李辉5');
INSERT INTO `test` VALUES (6, '李辉6');
INSERT INTO `test` VALUES (7, '李辉7');
INSERT INTO `test` VALUES (8, '李辉8');
INSERT INTO `test` VALUES (9, '李辉9');
INSERT INTO `test` VALUES (10, '李辉10');
INSERT INTO `test` VALUES (11, '李辉11');
INSERT INTO `test` VALUES (12, '李辉12');
INSERT INTO `test` VALUES (13, '李辉13');
INSERT INTO `test` VALUES (14, '李辉14');
INSERT INTO `test` VALUES (15, '李辉15');
INSERT INTO `test` VALUES (16, '李辉16');
INSERT INTO `test` VALUES (17, '李辉17');
INSERT INTO `test` VALUES (18, '李辉18');
INSERT INTO `test` VALUES (19, '李辉19');
INSERT INTO `test` VALUES (20, '李辉20');
INSERT INTO `test` VALUES (21, '李辉21');
INSERT INTO `test` VALUES (22, '李辉22');
INSERT INTO `test` VALUES (23, '李辉23');
INSERT INTO `test` VALUES (24, '李辉24');
INSERT INTO `test` VALUES (25, '李辉25');
INSERT INTO `test` VALUES (26, '李辉26');
INSERT INTO `test` VALUES (27, '李辉27');
INSERT INTO `test` VALUES (28, '李辉28');
INSERT INTO `test` VALUES (29, '李辉29');
INSERT INTO `test` VALUES (30, '李辉30');
INSERT INTO `test` VALUES (31, '李辉31');
INSERT INTO `test` VALUES (32, '李辉32');
INSERT INTO `test` VALUES (33, '李辉33');
INSERT INTO `test` VALUES (34, '李辉34');
INSERT INTO `test` VALUES (35, '李辉35');
INSERT INTO `test` VALUES (36, '李辉36');
INSERT INTO `test` VALUES (37, '李辉37');
INSERT INTO `test` VALUES (38, '李辉38');
INSERT INTO `test` VALUES (39, '李辉39');
INSERT INTO `test` VALUES (40, '李辉40');
INSERT INTO `test` VALUES (41, '李辉41');
INSERT INTO `test` VALUES (42, '李辉42');
INSERT INTO `test` VALUES (43, '李辉43');
INSERT INTO `test` VALUES (44, '李辉44');
INSERT INTO `test` VALUES (45, '李辉45');
INSERT INTO `test` VALUES (46, '李辉46');
INSERT INTO `test` VALUES (47, '李辉47');
INSERT INTO `test` VALUES (48, '李辉48');
INSERT INTO `test` VALUES (49, '李辉49');
INSERT INTO `test` VALUES (50, '李辉50');
INSERT INTO `test` VALUES (51, '李辉51');
INSERT INTO `test` VALUES (52, '李辉52');
INSERT INTO `test` VALUES (53, '李辉53');
INSERT INTO `test` VALUES (54, '李辉54');
INSERT INTO `test` VALUES (55, '李辉55');
INSERT INTO `test` VALUES (56, '李辉56');
INSERT INTO `test` VALUES (57, '李辉57');
INSERT INTO `test` VALUES (58, '李辉58');
INSERT INTO `test` VALUES (59, '李辉59');
INSERT INTO `test` VALUES (60, '李辉60');
INSERT INTO `test` VALUES (61, '李辉61');
INSERT INTO `test` VALUES (62, '李辉62');
INSERT INTO `test` VALUES (63, '李辉63');
INSERT INTO `test` VALUES (64, '李辉64');
INSERT INTO `test` VALUES (65, '李辉65');
INSERT INTO `test` VALUES (66, '李辉66');
INSERT INTO `test` VALUES (67, '李辉67');
INSERT INTO `test` VALUES (68, '李辉68');
INSERT INTO `test` VALUES (69, '李辉69');
INSERT INTO `test` VALUES (70, '李辉70');
INSERT INTO `test` VALUES (71, '李辉71');
INSERT INTO `test` VALUES (72, '李辉72');
INSERT INTO `test` VALUES (73, '李辉73');
INSERT INTO `test` VALUES (74, '李辉74');
INSERT INTO `test` VALUES (75, '李辉75');
INSERT INTO `test` VALUES (76, '李辉76');
INSERT INTO `test` VALUES (77, '李辉77');
INSERT INTO `test` VALUES (78, '李辉78');
INSERT INTO `test` VALUES (79, '李辉79');
INSERT INTO `test` VALUES (80, '李辉80');
INSERT INTO `test` VALUES (81, '李辉81');
INSERT INTO `test` VALUES (82, '李辉82');
INSERT INTO `test` VALUES (83, '李辉83');
INSERT INTO `test` VALUES (84, '李辉84');
INSERT INTO `test` VALUES (85, '李辉85');
INSERT INTO `test` VALUES (86, '李辉86');
INSERT INTO `test` VALUES (87, '李辉87');
INSERT INTO `test` VALUES (88, '李辉88');
INSERT INTO `test` VALUES (89, '李辉89');
INSERT INTO `test` VALUES (90, '李辉90');
INSERT INTO `test` VALUES (91, '李辉91');
INSERT INTO `test` VALUES (92, '李辉92');
INSERT INTO `test` VALUES (93, '李辉93');
INSERT INTO `test` VALUES (94, '李辉94');
INSERT INTO `test` VALUES (95, '李辉95');
INSERT INTO `test` VALUES (96, '李辉96');
INSERT INTO `test` VALUES (97, '李辉97');
INSERT INTO `test` VALUES (98, '李辉98');
INSERT INTO `test` VALUES (99, '李辉99');

-- ----------------------------
-- Table structure for wj_appeal
-- ----------------------------
DROP TABLE IF EXISTS `wj_appeal`;
CREATE TABLE `wj_appeal`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '诉求的id',
  `user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '诉求对应的用户id',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '诉求的标题',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '诉求的内容',
  `endorse_count` int(1) NULL DEFAULT 0 COMMENT '诉求点赞量',
  `comment_count` int(1) NULL DEFAULT 0 COMMENT '诉求的评论量',
  `browse_count` int(1) NULL DEFAULT 0 COMMENT '诉求浏览量',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `create_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建人(可以是名称 或者 uuid)',
  `update_date` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `update_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '修改人(可以是名称 或者 uuid)',
  `status` int(1) NULL DEFAULT 0 COMMENT '状态码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '诉求表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for wj_appeal_comment
-- ----------------------------
DROP TABLE IF EXISTS `wj_appeal_comment`;
CREATE TABLE `wj_appeal_comment`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '诉求评论的id',
  `about_comment` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '是否是评价评论的评论（有值代表评价了评论，反之为空就不是）',
  `appeal_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '诉求id',
  `user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '评论人的id',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '评论的内容',
  `url` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '评论所加的贴图（只能是一张）',
  `file_type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '评论贴图的文件类型(jpg,png,gif)',
  `file_size` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '评论贴图大小(KB)',
  `floor` int(1) NULL DEFAULT 1 COMMENT '评论的楼层',
  `endorse_count` int(1) NULL DEFAULT 0 COMMENT '评论的点赞数||赞同数',
  `comment_count` int(1) NULL DEFAULT 0 COMMENT '对评论评论的次数',
  `create_date` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `create_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建人',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `update_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '修改人',
  `status` int(1) NULL DEFAULT 0 COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '诉求-评论' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for wj_appeal_material
-- ----------------------------
DROP TABLE IF EXISTS `wj_appeal_material`;
CREATE TABLE `wj_appeal_material`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '诉求素材表的id',
  `appeal_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '诉求id',
  `url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '素材的url地址（存放在OSS）',
  `file_type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '素材格式(jpg,png.....)',
  `file_size` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '素材大小(KB 为单位)',
  `video_time` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '素材的时间，未来可能可以上传视频(冗余)',
  `sequence` int(1) NULL DEFAULT 1 COMMENT '顺序，应对用户分批次上传(冗余)',
  `create_date` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `create_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建人',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `update_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '修改人',
  `status` int(1) NULL DEFAULT 0 COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '诉求素材表-存储素材涉及的图片，或者大文件' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for wj_appeal_tag
-- ----------------------------
DROP TABLE IF EXISTS `wj_appeal_tag`;
CREATE TABLE `wj_appeal_tag`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `tag_name` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '标签的名字（冗余）',
  `dict_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '标签对应字典表的id',
  `explains` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '标签的解释（冗余）',
  `url` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '标签可能需要的地址值(冗余)',
  `status` int(1) NULL DEFAULT 0 COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '诉求-对应标签' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for wj_chat
-- ----------------------------
DROP TABLE IF EXISTS `wj_chat`;
CREATE TABLE `wj_chat`  (
  `chat_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'id',
  `type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'one' COMMENT '聊天的类型（one:一对一，more:一对多）',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标题',
  `people_number` int(1) NULL DEFAULT NULL COMMENT '限制人数',
  `url` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '聊天室的图片（一对一聊天就用双方的头像组合成一张，一对多就多张组合起来）',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `create_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `update_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `status` int(1) NULL DEFAULT 1 COMMENT '状态',
  PRIMARY KEY (`chat_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '聊天室（聊天列表）' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for wj_chat_record
-- ----------------------------
DROP TABLE IF EXISTS `wj_chat_record`;
CREATE TABLE `wj_chat_record`  (
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `chat_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '聊天室id',
  `user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户id',
  `chat_order` int(1) NOT NULL DEFAULT 1 COMMENT '聊天的次序（谁说在前，谁说在后）',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '聊天的内容',
  `type` int(1) NOT NULL DEFAULT 1 COMMENT '内容类型（1：文字 2：图片 3：视频）',
  `create_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `create_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `update_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `status` int(1) NULL DEFAULT 1 COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '聊天室-聊天记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for wj_chat_user
-- ----------------------------
DROP TABLE IF EXISTS `wj_chat_user`;
CREATE TABLE `wj_chat_user`  (
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `chat_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '聊天室id',
  `user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '聊天室对应的用户id（一对一，一对多）',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间（或者加入时间）',
  `quit_time` datetime(0) NULL DEFAULT NULL COMMENT '退出聊天的时间（一对多的聊天室可以退出群聊，需要个退出时间）',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `update_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `status` int(1) NULL DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '聊天室对应的用户' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for wj_dynamic
-- ----------------------------
DROP TABLE IF EXISTS `wj_dynamic`;
CREATE TABLE `wj_dynamic`  (
  `dynamic_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '动态id',
  `user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '关联的用户id',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '动态的内容（没有标题之类的）',
  `tag_dict_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '动态标签，对应的字典id',
  `url` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '动态所发素材的url地址，多个用逗号隔开',
  `file_type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '动态素材的类型，多个用逗号隔开',
  `file_size` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '动态素材的大小，多个用逗号隔开(KB)',
  `endorse_count` int(1) NULL DEFAULT 0 COMMENT '动态 点赞数||赞同数',
  `comment_count` int(1) NULL DEFAULT 0 COMMENT '动态 评论数',
  `browse_count` int(1) NULL DEFAULT 0 COMMENT '动态 浏览量',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `create_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建人',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `update_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '修改人',
  `status` int(1) NULL DEFAULT 1 COMMENT '状态（默认为1）',
  PRIMARY KEY (`dynamic_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '动态表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for wj_dynamic_comment
-- ----------------------------
DROP TABLE IF EXISTS `wj_dynamic_comment`;
CREATE TABLE `wj_dynamic_comment`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '动态评论id',
  `about_comment` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否是评价评论的评论（有值代表评价了评论，反之为空就不是）',
  `dynamic_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '对应的动态id',
  `user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '发起评论的用户id',
  `content` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '评论的内容',
  `content_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '评论的类型（1：文字 2：图片）',
  `file_size` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '素材的大小(KB)',
  `floor` int(1) NULL DEFAULT 1 COMMENT '评论的楼层',
  `endorse_count` int(1) NULL DEFAULT 0 COMMENT '评论的点赞数',
  `comment_count` int(1) NULL DEFAULT 0 COMMENT '评价评论的次数',
  `create_date` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `create_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建人',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `update_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '修改人',
  `status` int(1) NULL DEFAULT 1 COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '动态评论表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for wj_saying
-- ----------------------------
DROP TABLE IF EXISTS `wj_saying`;
CREATE TABLE `wj_saying`  (
  `say_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '语录id',
  `user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户id',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '标题',
  `cover` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '封面（URL地址）',
  `content` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '内容',
  `support_num` int(10) NULL DEFAULT 0 COMMENT '点赞数',
  `comment_num` int(10) NULL DEFAULT 0 COMMENT '评论数',
  `share_num` int(10) NULL DEFAULT 0 COMMENT '分享数',
  `create_date` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `create_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `update_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `status` int(1) NULL DEFAULT 1 COMMENT '状态[0:禁用，1:启用]',
  PRIMARY KEY (`say_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '语录表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for wj_user
-- ----------------------------
DROP TABLE IF EXISTS `wj_user`;
CREATE TABLE `wj_user`  (
  `user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户id',
  `nick_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '昵称',
  `real_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '真实姓名',
  `head_portrait` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '头像对应的URL地址',
  `sex` int(1) NULL DEFAULT 0 COMMENT '性别[0:未知;1:男;2:女]',
  `mobile` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '手机号',
  `company` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '公司',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '电子邮箱',
  `signature` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '个性签名（冗余）',
  `introduce` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '简介',
  `address` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '地址',
  `password` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '密码（冗余）',
  `identity_card` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '身份证号',
  `create_date` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `create_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建人',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `update_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '修改人',
  `status` int(1) NULL DEFAULT 1 COMMENT '状态[0:禁用，1:启用]',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wj_user
-- ----------------------------
INSERT INTO `wj_user` VALUES ('d7aa661b02ca41b485c9a4b33da73f27', '老腊肉不好吃', '', 'https://wx.qlogo.cn/mmopen/vi_32/PDXXwH95yyf0V6Bylibg1gBUwACpzTz3zyaGjs86q9GWLcAEW4MuVelia1oTbrx6icOCtU4LLwK1p22aibCibW4ZNUg/132', 1, '', '', '', '', NULL, '', '', '', '2019-11-26 06:06:04', 'd7aa661b02ca41b485c9a4b33da73f27', NULL, '', 1);

-- ----------------------------
-- Table structure for wj_user_track
-- ----------------------------
DROP TABLE IF EXISTS `wj_user_track`;
CREATE TABLE `wj_user_track`  (
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户id',
  `user_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户name',
  `ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ip',
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型(查询，删除，修改......)',
  `text` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `param` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '参数',
  `create_date` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_date` datetime(0) NULL DEFAULT NULL,
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tflag` int(2) NULL DEFAULT 0 COMMENT '标识(0:无,1:用户,2:诉求,3:动态,4:语录)',
  `target_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '目标id',
  `brand` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设备品牌',
  `model` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设备型号',
  `version` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '微信版本号',
  `system` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作系统及版本',
  `platform` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户端平台',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户足迹表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for wj_user_wx
-- ----------------------------
DROP TABLE IF EXISTS `wj_user_wx`;
CREATE TABLE `wj_user_wx`  (
  `user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户id',
  `wx_open_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户的唯一标识',
  `wx_union_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '（冗余）',
  `wx_app_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '小程序的后台',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '专门用来存储微信后台发送给我们的数据' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wj_user_wx
-- ----------------------------
INSERT INTO `wj_user_wx` VALUES ('d7aa661b02ca41b485c9a4b33da73f27', 'o_5lH48SmtOpxAYagXtLg5o1ShX4', NULL, 'wx968fb12f534b640b');

SET FOREIGN_KEY_CHECKS = 1;
