<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>...</title>
    <link rel="stylesheet" href="${re.contextPath}/plugin/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="${re.contextPath}/plugin/plugins/font-awesome/css/font-awesome.min.css" media="all"/>
    <link rel="stylesheet" href="${re.contextPath}/plugin/build/css/app.css" media="all"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="${re.contextPath}/plugin/layui/css/formSelects-v4.css">
    <link rel="stylesheet" href="${re.contextPath}/plugin/build/css/themes/default.css" media="all" id="skin" kit-skin/>
    <script type="text/javascript" src="${re.contextPath}/plugin/jquery/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="${re.contextPath}/plugin/layui/layui.all.js" charset="utf-8"></script>
    <script type="text/javascript" src="${re.contextPath}/plugin/tools/tool.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
    <style type="text/css">
        .layui-col-space15 {
            margin: 0 !important;
            margin-left: -7.5px !important;
        }
    </style>
</head>

<body layadmin-themealias="ocean-header">
<div class="layui-fluid">
    <div class="lenos-search title" style="margin-top: 10px">
        <div class="select">
            <button class="layui-btn layui-btn-sm layui-btn-warm icon-position-button btn-reload" style="float: right;"
                    id="refresh" onclick="location.reload()">
                <i class="layui-icon layui-icon-loading-1 layui-anim layui-anim-rotate layui-anim-loop"></i>
            </button>
        </div>
    </div>
    <div class="title" style="float: left;margin-top: 10px;"><span style="font-size:18px"><strong>用户数据</strong></span>
    </div>
    <div class="layui-row layui-col-space15">
        <div class="layui-col-sm6 layui-col-md3" style="margin-top: 10px;margin-bottom: 10px;margin-right: 100px">
            <div class="layui-card" style="background-color:#9fd7e5">
                <div class="layui-card-header">
                    <span style="font-size: 16px;"><i style="font-size: 30px;" class="layui-icon
                    layui-icon-form"></i><strong>今日新增</strong></span>
                </div>
                <div class="layui-card-body layuiadmin-card-list">
                    <p class="layuiadmin-big-font" style="font-size: 20px;" id="todayAddUser"></p>
                </div>
            </div>
        </div>
        <div class="layui-col-sm6 layui-col-md3" style="margin-top: 10px;margin-bottom: 10px;margin-right: 100px">
            <div class="layui-card" style="background-color:#C9DEF5">
                <div class="layui-card-header">
                    <span style="font-size: 16px;"><i style="font-size: 30px;" class="layui-icon
                    layui-icon-form"></i><strong>用户总数</strong></span>
                </div>
                <div class="layui-card-body layuiadmin-card-list">
                    <p class="layuiadmin-big-font" style="font-size: 20px;" id="totalUser"></p>
                </div>
            </div>
        </div>
        <div class="layui-col-sm6 layui-col-md3" style="margin-top: 10px;margin-bottom: 10px;">
            <div class="layui-card" style="background-color:#69BC80">
                <div class="layui-card-header">
                    <span style="font-size: 16px;"><i style="font-size: 30px;" class="layui-icon
                    layui-icon-form"></i><strong>绑定手机用户数</strong></span>
                </div>
                <div class="layui-card-body layuiadmin-card-list">
                    <p class="layuiadmin-big-font" style="font-size: 20px;" id="binding"></p>
                </div>
            </div>
        </div>
    </div>
    <div class="title" style="float: left;margin-right: 50px"><span style="font-size:18px"><strong>问答数据</strong></span>
    </div>
    <div class="layui-row layui-col-space15">
        <div class="layui-col-sm6 layui-col-md3" style="margin-top: 10px;margin-bottom: 10px;margin-right: 100px">
            <div class="layui-card" style="background-color:#9fd7e5">
                <div class="layui-card-header">
                    <span style="font-size: 16px;"><i style="font-size: 30px;" class="layui-icon
                    layui-icon-form"></i><strong>今日新增问题数</strong></span>
                </div>
                <div class="layui-card-body layuiadmin-card-list">
                    <p class="layuiadmin-big-font" style="font-size: 20px;" id="todayAddQuestion"></p>
                </div>
            </div>
        </div>
        <div class="layui-col-sm6 layui-col-md3" style="margin-top: 10px;margin-bottom: 10px;margin-right: 100px">
            <div class="layui-card" style="background-color:#C9DEF5">
                <div class="layui-card-header">
                    <span style="font-size: 16px;"><i style="font-size: 30px;" class="layui-icon
                    layui-icon-form"></i><strong>问题总数</strong></span>
                </div>
                <div class="layui-card-body layuiadmin-card-list">
                    <p class="layuiadmin-big-font" style="font-size: 20px;" id="totalQuestion"></p>
                </div>
            </div>
        </div>
        <div class="layui-col-sm6 layui-col-md3" style="margin-top: 10px;margin-bottom: 10px;">
            <div class="layui-card" style="background-color:#69BC80">
                <div class="layui-card-header">
                    <span style="font-size: 16px;"><i style="font-size: 30px;" class="layui-icon
                    layui-icon-form"></i><strong>回答总数</strong></span>
                </div>
                <div class="layui-card-body layuiadmin-card-list">
                    <p class="layuiadmin-big-font" style="font-size: 20px;" id="totalReview"></p>
                </div>
            </div>
        </div>
    </div>
    <div class="title" style="float: left;margin-right: 50px"><span style="font-size:18px"><strong>课程数据</strong></span>
    </div>
    <div class="layui-row layui-col-space15">
        <div class="layui-col-sm6 layui-col-md3" style="margin-top: 10px;margin-bottom: 10px;margin-right: 100px">
            <div class="layui-card" style="background-color:#9fd7e5">
                <div class="layui-card-header">
                    <span style="font-size: 16px;"><i style="font-size: 30px;" class="layui-icon
                    layui-icon-form"></i><strong>今日新增课程小节数</strong></span>
                </div>
                <div class="layui-card-body layuiadmin-card-list">
                    <p class="layuiadmin-big-font" style="font-size: 20px;" id="todayAddSection"></p>
                </div>
            </div>
        </div>
        <div class="layui-col-sm6 layui-col-md3" style="margin-top: 10px;margin-bottom: 10px;margin-right: 100px">
            <div class="layui-card" style="background-color:#C9DEF5">
                <div class="layui-card-header">
                    <span style="font-size: 16px;"><i style="font-size: 30px;" class="layui-icon
                    layui-icon-form"></i><strong>课程小节总数</strong></span>
                </div>
                <div class="layui-card-body layuiadmin-card-list">
                    <p class="layuiadmin-big-font" style="font-size: 20px;" id="totalSection"></p>
                </div>
            </div>
        </div>
        <div class="layui-col-sm6 layui-col-md3" style="margin-top: 10px;margin-bottom: 10px;">
            <div class="layui-card" style="background-color:#69BC80">
                <div class="layui-card-header">
                    <span style="font-size: 16px;"><i style="font-size: 30px;" class="layui-icon
                    layui-icon-form"></i><strong>课程总数</strong></span>
                </div>
                <div class="layui-card-body layuiadmin-card-list">
                    <p class="layuiadmin-big-font" style="font-size: 20px;" id="totalCourse"></p>
                </div>
            </div>
        </div>
    </div>
    <div class="title" style="float: left;margin-right: 50px"><span style="font-size:18px"><strong>积分数据</strong></span>
    </div>
    <div class="layui-row layui-col-space15">
        <div class="layui-col-sm6 layui-col-md3" style="margin-top: 10px;margin-bottom: 10px;margin-right: 100px">
            <div class="layui-card" style="background-color:#9fd7e5">
                <div class="layui-card-header">
                    <span style="font-size: 16px;"><i style="font-size: 30px;" class="layui-icon
                    layui-icon-form"></i><strong>今日新增积分数</strong></span>
                </div>
                <div class="layui-card-body layuiadmin-card-list">
                    <p class="layuiadmin-big-font" style="font-size: 20px;" id="todayAddIntegral"></p>
                </div>
            </div>
        </div>
        <div class="layui-col-sm6 layui-col-md3" style="margin-top: 10px;margin-bottom: 10px;margin-right: 100px">
            <div class="layui-card" style="background-color:#C9DEF5">
                <div class="layui-card-header">
                    <span style="font-size: 16px;"><i style="font-size: 30px;" class="layui-icon
                    layui-icon-form"></i><strong>积分发放总数</strong></span>
                </div>
                <div class="layui-card-body layuiadmin-card-list">
                    <p class="layuiadmin-big-font" style="font-size: 20px;" id="totalGrantIntegral"></p>
                </div>
            </div>
        </div>
        <div class="layui-col-sm6 layui-col-md3" style="margin-top: 10px;margin-bottom: 10px;">
            <div class="layui-card" style="background-color:#69BC80">
                <div class="layui-card-header">
                    <span style="font-size: 16px;"><i style="font-size: 30px;" class="layui-icon
                    layui-icon-form"></i><strong>剩余积分总数</strong></span>
                </div>
                <div class="layui-card-body layuiadmin-card-list">
                    <p class="layuiadmin-big-font" style="font-size: 20px;" id="totalResidueIntegral"></p>
                </div>
            </div>
        </div>
    </div>
    <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal"
            style="display: none" id="jm">
    </button>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" style="width: 100%;margin: 0;height: 100%;" role="document">
            <div class="modal-content container-fluid" style="height: 100%;background: url(/images/logo_bg_white.jpg)
            ;background-size: cover">
                <div class="jumbotron" style="margin-top: 5%;background:none;">
                    <h2 style="font-weight: bold;background-image:-webkit-linear-gradient(right,#22913b,#f29438);
                    -webkit-background-clip:text;-webkit-text-fill-color:transparent;display: inline-block;">
                        欢迎进入趣农天下后台管理系统</h2>
                    <p><em>祝您工作顺利</em></p>
                    <p class="btn-p" style="display:none;">
                        <button class="btn btn-success btn-lg" role="button" data-dismiss="modal"
                                style="display:none" id="btn-h">
                            <i class="glyphicon glyphicon-hand-right" style="vertical-align: text-top;"></i>
                        </button>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <style id="LAY_layadmin_theme">.layui-side-menu, .layadmin-pagetabs .layui-tab-title li:after, .layadmin-pagetabs .layui-tab-title li.layui-this:after, .layui-layer-admin .layui-layer-title, .layadmin-side-shrink .layui-side-menu .layui-nav > .layui-nav-item > .layui-nav-child {
            background-color: #344058 !important;
        }

        .layui-nav-tree .layui-this, .layui-nav-tree .layui-this > a, .layui-nav-tree .layui-nav-child dd.layui-this, .layui-nav-tree .layui-nav-child dd.layui-this a {
            background-color: #1E9FFF !important;
        }

        .layui-layout-admin .layui-logo {
            background-color: #0085E8 !important;
        }

        .layui-layout-admin .layui-header {
            background-color: #1E9FFF;
        }

        .layui-layout-admin .layui-header a, .layui-layout-admin .layui-header a cite {
            color: #f8f8f8;
        }

        .layui-layout-admin .layui-header a:hover {
            color: #fff;
        }

        .layui-layout-admin .layui-header .layui-nav .layui-nav-more {
            border-top-color: #fbfbfb;
        }

        .layui-layout-admin .layui-header .layui-nav .layui-nav-mored {
            border-color: transparent;
            border-bottom-color: #fbfbfb;
        }

        .layui-layout-admin .layui-header .layui-nav .layui-this:after, .layui-layout-admin .layui-header .layui-nav-bar {
            background-color: #fff;
            background-color: rgba(255, 255, 255, .5);
        }

        .layadmin-pagetabs .layui-tab-title li:after {
            display: none;
        }

        .layui-col-space15 {
            display: none;
        }

        .title {
            display: none;
        }

        .btn-success {
            background-color: #22913b;
        }
    </style>
    <script>
        $(function () {
            $('#jm').click();
            $.ajax({
                url: '/question/load',
                type: 'get',
                dataType: 'json',
                success: function (d) {
                    $('#todayAddUser').html('<strong>' + d.todayAddUser + '</strong>')
                    $('#totalUser').html('<strong>' + d.totalUser + '</strong>')
                    $('#binding').html('<strong>' + d.binding + '</strong>')
                    $('#todayAddQuestion').html('<strong>' + d.todayAddQuestion + '</strong>')
                    $('#totalQuestion').html('<strong>' + d.totalQuestion + '</strong>')
                    $('#totalReview').html('<strong>' + d.totalReview + '</strong>')
                    $('#todayAddSection').html('<strong>' + d.todayAddSection + '</strong>')
                    $('#totalSection').html('<strong>' + d.totalSection + '</strong>')
                    $('#totalCourse').html('<strong>' + d.totalCourse + '</strong>')
                    $('#todayAddIntegral').html('<strong>' + d.todayAddIntegral + '</strong>')
                    $('#totalGrantIntegral').html('<strong>' + d.totalGrantIntegral + '</strong>')
                    $('#totalResidueIntegral').html('<strong>' + d.totalResidueIntegral + '</strong>')
                    setTimeout(function () {
                        $('#btn-h').click();
                        $('.title').show();
                        $('.layui-col-space15').show();
                    }, 1000)
                }
            })
        })
    </script>
</body>
</html>
