package com.huixi.weiju.dao;

import com.huixi.weiju.pojo.test;
import java.util.List;

/**
 *  测试用的方法
 * @Author 叶秋 
 * @Date 2020/1/10 14:18
 * @param 
 * @return 
 **/
public interface testMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(test record);

    test selectByPrimaryKey(Integer id);

    List<test> selectAll();

    int updateByPrimaryKey(test record);
}