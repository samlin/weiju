package com.huixi.weiju.dao.usermodel;

import com.huixi.weiju.pojo.userModel.DO.UserWxDO;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserWxDao {
    int deleteByPrimaryKey(String userId);

    int insert(UserWxDO record);

    int insertCondition(UserWxDO userWxDO);

    UserWxDO selectByPrimaryKey(String userId);

    UserWxDO selectByPrimaryOpenId(String wxOpenId);

    List<UserWxDO> selectAll();

    int updateByPrimaryKey(UserWxDO record);
}