package com.huixi.weiju.exception.enums;

/**
 *  与动态相关的 状态码
 * @Author 李辉
 * @Date 2019/9/18 15:37
 * @param
 * @return
 **/
public enum DynamicCode implements IErrorCode{

    /**
     * DYNAMIC 100 error code enum.
     */
    DYNAMIC100(100, "参数异常"),
    /**
     * DYNAMIC 401 error code enum.
     */
    DYNAMIC401(401, "无访问权限"),
    /**
     * DYNAMIC 500 error code enum.
     */
    DYNAMIC500(500, "未知异常"),
    /**
     * DYNAMIC 403 error code enum.
     */
    DYNAMIC403(403, "无权访问"),
    /**
     * DYNAMIC 404 error code enum.
     */
    DYNAMIC404(404, "找不到指定资源");

    private long code;
    private String message;

    DynamicCode(long code, String message){
        this.code = code;
        this.message=  message;
    }


    @Override
    public long getCode() {
        return 0;
    }

    @Override
    public String getMessage() {
        return null;
    }
}
