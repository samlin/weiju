package com.huixi.weiju.controller;

import com.huixi.weiju.dao.testMapper;
import com.huixi.weiju.exception.util.CommonResult;
import com.huixi.weiju.pojo.test;
import com.huixi.weiju.util.AlibabaOSS;
import org.springframework.cache.annotation.CachePut;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 *  测试用的
 * @Author 叶秋
 * @Date 2020/1/10 14:19
 * @param
 * @return
 **/
@CrossOrigin
@RestController
@RequestMapping("/test")
public class TestController {

    @Resource
    private testMapper testMapper;

    @Resource
    private AlibabaOSS alibabaOSS;

    /**
      * 查询所有Test 表中的值
      *@author: 李辉
      *@date: 2019/11/16 14:04
      *@param:
      *@return:
      */
    @CachePut(value = "testAll")
    @GetMapping(value = "/selectAllTest")
    public CommonResult selectAllTest(){

//        OSS ossClient = alibabaOSS.getOssClient();
//        ossClient.putObject(alibabaOSS.getBucketName(),"1.mp4", new File("C:\\Users\\14163\\Pictures\\风铃_55.mp4"));
//        ossClient.shutdown();

        List<test> tests = testMapper.selectAll();

        System.out.println("Hello test test");

        return CommonResult.success(tests);

    }


}
