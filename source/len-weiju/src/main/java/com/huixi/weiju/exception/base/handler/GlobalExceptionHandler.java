package com.huixi.weiju.exception.base.handler;


import com.huixi.weiju.exception.util.CommonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author libe
 * @create 2019/9/9 16:27
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    /*
     * @ControllerAdvice
     * 单使用@ExceptionHandler，只能在当前Controller中处理异常。但当配合@ControllerAdvice一起使用的时候，就可以摆脱那个限制了。
     * 1.全局异常处理
     * 2.全局数据绑定
     * 3.全局数据预处理
     */

    /**
     * 全局异常.
     * @param e [异常类]
     * @return ResultCode []
     */
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public CommonResult exception(Exception e) {
        log.info("保存全局异常信息 ex={}", e.getMessage(), e);
        return new CommonResult(e);
    }

}
