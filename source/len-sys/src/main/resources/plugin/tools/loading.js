function showProgress() {
    $('#shade').show()
    var startNum = 0;
    var maxNum = 95;
    var speed = 80;
    var time = setInterval(function () {
        startNum++
        if (startNum <= maxNum) {
            $('#shade .progress-bar')[0].style.width = startNum + '%'
            $('#img-loading')[0].style.left = (startNum - 11) + '%'
        } else {
            clearInterval(time)
        }
    }, speed)
}


function hideProgress() {
    $('#shade .progress-bar')[0].style.width = "100%"
    $('#img-loading')[0].style.left = '89%'
    setTimeout(function () {
        $('#shade').hide()
    }, 100)
}

