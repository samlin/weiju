<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>${AppName}</title>
    <link rel="icon" href="https://h5.qntianxia.com/icon/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="${re.contextPath}/plugin/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="${re.contextPath}/plugin/plugins/font-awesome/css/font-awesome.min.css" media="all"/>
    <link rel="stylesheet" href="${re.contextPath}/plugin/build/css/app.css" media="all"/>
    <link rel="stylesheet" href="${re.contextPath}/plugin/build/css/themes/default.css" media="all" id="skin" kit-skin/>
    <style>
        <#--前端无聊美化ing-->
        .layui-footer {
            background-color: #2F4056;
        }

        .layui-side-scroll {
            border-right: 3px solid #009688;
        }

        .time-box {
            position: absolute;
            left: 40%;
            top: 50%;
            margin-top: -10px;
            color: #fff;
            font-size: 18px;
        }

        .time-box i {
            font-size: 18px;
        }
    </style>
</head>

<body class="kit-theme">
<div class="layui-layout layui-layout-admin kit-layout-admin" style="user-select: none">
    <div class="layui-header site-doc-anim">
        <div class="layui-logo" style="width: 280px"><img src="static/images/qntx_logo_white.png">
            <a href="javascript:showVersion('1.0.2')" style="bottom: -15px;color:#ffffff">V 1.0.2</a>
        </div>
        <#--<div class="time-box" id="showtime"></div>-->
        <div class="layui-logo kit-logo-mobile"></div>
        <ul class="layui-nav layui-layout-right kit-nav">
            <li class="layui-nav-item">
                <a href="javascript:;">
                    <i class="layui-icon">&#xe63f;</i> 皮肤</a>
                </a>
                <dl class="layui-nav-child skin">
                    <dd><a href="javascript:;" data-skin="default" style="color:#393D49;"><i
                                    class="layui-icon">&#xe658;</i> 默认</a></dd>
                    <dd><a href="javascript:;" data-skin="orange" style="color:#ff6700;"><i
                                    class="layui-icon">&#xe658;</i> 橘子橙</a></dd>
                    <dd><a href="javascript:;" data-skin="green" style="color:#00a65a;"><i
                                    class="layui-icon">&#xe658;</i> 春天绿</a></dd>
                    <dd><a href="javascript:;" data-skin="pink" style="color:#FA6086;"><i
                                    class="layui-icon">&#xe658;</i> 少女粉</a></dd>
                    <dd><a href="javascript:;" data-skin="blue.1" style="color:#00c0ef;"><i
                                    class="layui-icon">&#xe658;</i> 天空蓝</a></dd>
                    <dd><a href="javascript:;" data-skin="red" style="color:#dd4b39;"><i class="layui-icon">&#xe658;</i>
                            枫叶红</a></dd>
                </dl>
            </li>
            <li class="layui-nav-item">
                <a href="javascript:;">
                    <#assign currentUser = Session["currentPrincipal"]>
                    <img src="${re.contextPath}/images/${currentUser.photo}"
                         class="layui-nav-img">${currentUser.username}
                </a>
                <dl class="layui-nav-child">
                    <dd><a href="javascript:;" kit-target
                           data-options="{url:'/person',icon:'&#xe658;',title:'基本资料',id:'966'}"><span>基本资料</span></a>
                    </dd>
                    <dd><a href="javascript:showRepass('${currentUser.id}');">修改密码</a></dd>
                </dl>
            </li>
            <li class="layui-nav-item"><a href="logout"><i class="fa fa-sign-out" aria-hidden="true"></i> 注销</a></li>
        </ul>
    </div>

    <#macro tree data start end>
        <#if (start=="start")>
            <div class="layui-side layui-nav-tree layui-bg-black kit-side">
            <div class="layui-side-scroll">
            <div class="kit-side-fold"><i class="fa fa-navicon" aria-hidden="true"></i></div>
            <ul class="layui-nav layui-nav-tree" lay-filter="kitNavbar" kit-navbar>
        </#if>
        <#list data as child>
            <#if child.children?size gt 0>
                <li class="layui-nav-item">
                    <a class="" href="javascript:;"><i aria-hidden="true"
                                                       class="layui-icon">${child.icon}</i><span> ${child.name}</span></a>
                    <dl class="layui-nav-child">
                        <@tree data=child.children start="" end=""/>
                    </dl>
                </li>
            <#else>
                <dd>
                    <a href="javascript:;" kit-target
                       data-options="{url:'${child.url}',icon:'${child.icon}',title:'${child.name}',id:'${child.num?c}'}">
                        <i class="layui-icon">${child.icon}</i><span> ${child.name}</span></a>
                </dd>
            </#if>
        </#list>
        <#if (end=="end")>
            </ul>
            </div>
            </div>
        </#if>
    </#macro>
    <@tree data=menu start="start" end="end"/>
    <div class="layui-body" <#--style="border:1px solid red;padding-bottom:0;"--> id="container">
        <!-- 内容主体区域 -->
        <div style="padding: 15px;"><i class="layui-icon layui-anim layui-anim-rotate layui-anim-loop">&#xe63e;</i>
            请稍等...
        </div>
    </div>

    <div class="layui-footer">
        <!-- 底部固定区域 -->
    </div>
</div>
<script src="${re.contextPath}/plugin/layui/layui.js"></script>
<script src="${re.contextPath}/plugin/tools/main.js"></script>
<script type="text/javascript" src="${re.contextPath}/plugin/jquery/jquery-3.2.1.min.js"></script>
<script>
    // getD();

    function showRepass(id) {
        console.log(id)
        layer.open({
            id: 'show-repass',
            type: 2,
            area: ['500px', '350px'],
            fix: false,
            shadeClose: true,
            shade: 0.4,
            title: '修改密码',
            content: '/user/goRePass?id=' + id
        });
    }

    function showVersion(vNo) {
        layer.open({
            id: 'show-version',
            type: 2,
            area: ['700px', '500px'],
            fix: false,
            shadeClose: true,
            shade: 0.4,
            title: '版本信息',
            content: '/user/showVersion?vNo=' + vNo
        });
    }

    function getD() {
        var date = new Date();
        var year = date.getFullYear();
        var month = (date.getMonth() + 1) < 10 ? '0' + (date.getMonth() + 1) : (date.getMonth() + 1);
        var day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
        var week = date.getDay();
        var hour = date.getHours() < 10 ? '0' + date.getHours() : date.getHours();
        var minute = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
        var second = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
        switch (week) {
            case 0:
                week = '星期日'
                break;
            case 1:
                week = '星期一'
                break;
            case 2:
                week = '星期二'
                break;
            case 3:
                week = '星期三'
                break;
            case 4:
                week = '星期四'
                break;
            case 5:
                week = '星期五'
                break;
            case 6:
                week = '星期六'
                break;
        }
        var dateStr = year + '年' + month + '月' + day + '日' + ' ' + hour + '时' + minute + '分' + second + '秒' +
            ' <span style="font-size: 16px;margin-left:8px">' + week + '</span>';
        var div1 = document.getElementById('showtime');
        div1.innerHTML = '<i class="layui-icon">&#xe60e;</i> ' + dateStr;
        setInterval('getD();', 1000);
    }

</script>
</body>

</html>
