<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>版本信息</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${re.contextPath}/plugin/layui/css/layui.css">
</head>

<body>
<div class="x-body">
    <div style="width:100%;height:100%;overflow: auto;user-select: none;">
        <#if list?exists>
            <#list list as v>
                <div class="layui-collapse">
                    <div class="layui-colla-item">
                        <h2 class="layui-colla-title">
                            版本 ${v.versionNo}<span
                                    style="float: right">● ${v.createTime?string("yyyy-MM-dd HH:mm:ss")}</span>
                        </h2>
                        <div class="layui-colla-content" id="versionBox">
                            ${v.versionDesc}
                        </div>
                    </div>
                </div>
            </#list>
        </#if>
        <div style="height: 60px"></div>
    </div>
    <div style="width: 100%;height: 55px;background-color: white;border-top:1px solid #e6e6e6;
  position: fixed;bottom: 1px;margin-left:-20px;">
        <div class="layui-form-item" style=" float: right;margin-right: 20px;margin-top: 8px">
            <button class="layui-btn layui-btn-primary" id="close">
                取消
            </button>
        </div>
    </div>
</div>
<script type="text/javascript" src="${re.contextPath}/plugin/jquery/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="${re.contextPath}/plugin/layui/layui.all.js" charset="utf-8"></script>
<script>
    layui.use(['form', 'layer', 'element'], function () {
        $ = layui.jquery;
        var form = layui.form
            , layer = layui.layer,
            element = layui.element;

        $('#close').click(function () {
            var index = parent.layer.getFrameIndex(window.name);
            parent.layer.close(index);
        });
    });
</script>
</body>

</html>
