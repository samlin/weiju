<div class="shade" id="shade">
    <div class="progress">
        <div class="progress-bar active " role="progressbar"
             style="width: 0%">
            <img src="/images/loading-whale.gif" width="50px" id="img-loading">
        </div>
    </div>
</div>