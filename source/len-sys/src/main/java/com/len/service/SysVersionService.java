package com.len.service;

import com.len.base.BaseService;
import com.len.dto.ListVersionDTO;
import com.len.entity.SysVersion;

import java.util.List;

public interface SysVersionService extends BaseService<SysVersion, String> {

    SysVersion getVersionByNo(String vNo);

    List<ListVersionDTO> listVersion();
}
