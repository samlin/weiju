package com.len.service.impl;

import com.len.base.BaseMapper;
import com.len.base.impl.BaseServiceImpl;
import com.len.dto.ListVersionDTO;
import com.len.entity.SysVersion;
import com.len.mapper.SysVersionMapper;
import com.len.service.SysVersionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SysVersionServiceImpl extends BaseServiceImpl<SysVersion, String> implements SysVersionService {

    @Autowired
    SysVersionMapper versionMapper;

    @Override
    public BaseMapper<SysVersion, String> getMappser() {
        return versionMapper;
    }

    @Override
    public SysVersion getVersionByNo(String vNo) {
        return versionMapper.getVersionByNo(vNo);
    }

    @Override
    public List<ListVersionDTO> listVersion() {
        return versionMapper.listVersion();
    }
}
