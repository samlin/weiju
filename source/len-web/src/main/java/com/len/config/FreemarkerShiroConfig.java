package com.len.config;

import com.jagregory.shiro.freemarker.ShiroTags;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author zhuxiaomeng
 * @date 2018/1/16.
 * @email 154040976@qq.com
 */
@Component
public class FreemarkerShiroConfig implements InitializingBean {
    @Value("${app.appName}")
    private String appName;

    @Autowired
    private freemarker.template.Configuration configuration;

    @Override
    public void afterPropertiesSet() throws Exception {
        configuration.setSharedVariable("shiro", new ShiroTags());
    }

    @PostConstruct
    public void setConfigure() throws Exception {
        configuration.setSharedVariable("AppName", appName);
    }
}
